<?php
  if(!isset($_GET['buscar']) && !isset($_GET['categoria'])){
    header("location:index.php");
  }
  include("inc/header.php");
  include("inc/conexion.php");
  ?>
  <div class="text-left">
    <h4>Resultados de la busqueda</h4>
    <hr>
    <div class="row justify-content-between">
  <?php

  //Empieza contenido de la página
  if(isset($_GET['buscar'])){
    $sql = "SELECT p.id_producto, nombre_producto, precio, direccion_imagen";
    $sql .= " FROM productos p";
    $sql .= " INNER JOIN galerias g";
    $sql .= " ON p.id_producto = g.id_producto";
    $sql .= " WHERE UPPER(nombre_producto) LIKE UPPER('%".$_GET['buscar']."%') AND slider = 0";
    $query = $conn->prepare($sql);
    $query->execute();
    $flag = true;

    while($row = $query->fetch(PDO::FETCH_ASSOC)){ ?>
      <div class="card border-dark col-md-4">
        <img class="card-img-top" src="<?php echo "img/".$row['DIRECCION_IMAGEN']; ?>">
        <div class="card-body">
          <h4 class="card-title"><?php echo $row['NOMBRE_PRODUCTO']; ?></h4>
          <div class="mt-4">
            <p><?php echo $row['PRECIO']."MXN"; ?>
              <a href="articulo.php?id=<?php echo $row['ID_PRODUCTO']; ?>">
                <button type="button" class="btn-sm btn-success float-right">Comprar</button>
              </a>
            </p>
          </div>
        </div>
      </div>

    <?php
      $flag = false;
        }
      if($flag){
        echo "No se ha encontrado ningún resultado para: " . $_GET['buscar'];
      }
    ?>
      </div>
    </div>
  <?php } if(isset($_GET['categoria'])){
    $sql = "SELECT p.id_producto, nombre_producto, precio, direccion_imagen, categoria";
    $sql .= " FROM productos p";
    $sql .= " INNER JOIN galerias g";
    $sql .= " ON p.id_producto = g.id_producto";
    $sql .= " WHERE UPPER(categoria) LIKE UPPER('%".$_GET['categoria']."%') AND slider = 0";
    $query = $conn->prepare($sql);
    $query->execute();
    $flag = true;

    while($row = $query->fetch(PDO::FETCH_ASSOC)){ ?>
      <div class="card border-dark col-md-4">
        <img class="card-img-top" src="<?php echo "img/".$row['DIRECCION_IMAGEN']; ?>">
        <div class="card-body">
          <h4 class="card-title"><?php echo $row['NOMBRE_PRODUCTO']; ?></h4>
          <div class="mt-4">
            <p><?php echo $row['PRECIO']."MXN"; ?>
              <a href="articulo.php?id=<?php echo $row['ID_PRODUCTO']; ?>">
                <button type="button" class="btn-sm btn-success float-right">Comprar</button>
              </a>
            </p>
          </div>
        </div>
      </div>

    <?php
      $flag = false;
        }
      if($flag){
        echo "No se ha encontrado ningún resultado para: " . $_GET['categoria'];
      }
    ?>
      </div>
    </div>
  <?php } ?>
<?php
  include("inc/jquery.php");
  include("inc/footer.php");
?>
