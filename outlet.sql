
DROP TABLE Empleados CASCADE CONSTRAINTS PURGE;



DROP TABLE Puestos CASCADE CONSTRAINTS PURGE;



DROP TABLE Favoritos CASCADE CONSTRAINTS PURGE;



DROP TABLE Ref_Pago CASCADE CONSTRAINTS PURGE;



DROP TABLE Galerias CASCADE CONSTRAINTS PURGE;



DROP TABLE Productos_Pedidos CASCADE CONSTRAINTS PURGE;



DROP TABLE Pedidos CASCADE CONSTRAINTS PURGE;



DROP TABLE Modalidades CASCADE CONSTRAINTS PURGE;



DROP TABLE Inventario CASCADE CONSTRAINTS PURGE;



DROP TABLE Bodegas CASCADE CONSTRAINTS PURGE;



DROP TABLE Carrito CASCADE CONSTRAINTS PURGE;



DROP TABLE Productos CASCADE CONSTRAINTS PURGE;



DROP TABLE Usuarios CASCADE CONSTRAINTS PURGE;



CREATE TABLE Bodegas
(
	id_bodega            INTEGER NOT NULL ,
	nombre_bodega        VARCHAR2(20) NOT NULL ,
	direccion_bodega     VARCHAR2(250) NOT NULL ,
	pais_bodega          VARCHAR2(50) NOT NULL ,
	esado_bodega         VARCHAR2(50) NOT NULL ,
	ciudad_bodega        VARCHAR2(50) NOT NULL 
);



CREATE UNIQUE INDEX XPKBodegas ON Bodegas
(id_bodega   ASC);



ALTER TABLE Bodegas
	ADD CONSTRAINT  XPKBodegas PRIMARY KEY (id_bodega);



CREATE TABLE Carrito
(
	id_usuario           INTEGER NOT NULL ,
	id_producto          INTEGER NOT NULL ,
	cantidad             INTEGER NOT NULL 
);



CREATE UNIQUE INDEX XPKCarrito ON Carrito
(id_usuario   ASC,id_producto   ASC);



ALTER TABLE Carrito
	ADD CONSTRAINT  XPKCarrito PRIMARY KEY (id_usuario,id_producto);



CREATE TABLE Empleados
(
	id_empleado          CHAR(18) NOT NULL ,
	usuario              VARCHAR2(40) NOT NULL ,
	contrasena           VARCHAR2(40) NOT NULL ,
	nombre_empleado      VARCHAR2(100) NOT NULL ,
	apellido_empleado    VARCHAR2(100) NOT NULL ,
	fecha_contrato       DATE DEFAULT  CURRENT_DATE  NOT NULL ,
	id_bodega            INTEGER NOT NULL ,
	id_puesto            INTEGER NOT NULL ,
	comision             FLOAT NOT NULL 
);



CREATE UNIQUE INDEX XPKEmpleados ON Empleados
(id_empleado   ASC);



ALTER TABLE Empleados
	ADD CONSTRAINT  XPKEmpleados PRIMARY KEY (id_empleado);



CREATE TABLE Favoritos
(
	id_usuario           INTEGER NOT NULL ,
	id_producto          INTEGER NOT NULL 
);



CREATE UNIQUE INDEX XPKFavoritos ON Favoritos
(id_usuario   ASC,id_producto   ASC);



ALTER TABLE Favoritos
	ADD CONSTRAINT  XPKFavoritos PRIMARY KEY (id_usuario,id_producto);



CREATE TABLE Galerias
(
	id_imagen            CHAR(18) NOT NULL ,
	direccion_imagen     VARCHAR2(100) NOT NULL ,
	id_producto          INTEGER NOT NULL ,
	slider               NUMBER(1,0) NULL 
);



CREATE UNIQUE INDEX XPKGalerias ON Galerias
(id_imagen   ASC,id_producto   ASC);



ALTER TABLE Galerias
	ADD CONSTRAINT  XPKGalerias PRIMARY KEY (id_imagen,id_producto);



CREATE TABLE Inventario
(
	id_producto          INTEGER NOT NULL ,
	id_bodega            INTEGER NOT NULL ,
	cantidad             INTEGER NOT NULL 
);



CREATE UNIQUE INDEX XPKProductos_Bodegas ON Inventario
(id_producto   ASC,id_bodega   ASC);



ALTER TABLE Inventario
	ADD CONSTRAINT  XPKProductos_Bodegas PRIMARY KEY (id_producto,id_bodega);



CREATE TABLE Modalidades
(
	id_modalidad         INTEGER NOT NULL ,
	descripcion          VARCHAR2(60) NOT NULL 
);



CREATE UNIQUE INDEX XPKModalidades ON Modalidades
(id_modalidad   ASC);



ALTER TABLE Modalidades
	ADD CONSTRAINT  XPKModalidades PRIMARY KEY (id_modalidad);



CREATE TABLE Pedidos
(
	id_pedido            INTEGER NOT NULL ,
	destino              VARCHAR2(350) NOT NULL ,
	paqueteria           VARCHAR2(20) NOT NULL ,
	detalles             VARCHAR2(100) NULL ,
	id_usuario           INTEGER NULL ,
	id_modalidad         INTEGER NULL 
);



CREATE UNIQUE INDEX XPKPedidos ON Pedidos
(id_pedido   ASC);



ALTER TABLE Pedidos
	ADD CONSTRAINT  XPKPedidos PRIMARY KEY (id_pedido);



CREATE TABLE Productos
(
	id_producto          INTEGER NOT NULL ,
	nombre_producto      VARCHAR2(60) NOT NULL ,
	categoria            VARCHAR2(40) NOT NULL ,
	color                VARCHAR2(20) NOT NULL ,
	precio               FLOAT NOT NULL ,
	precio_fab           FLOAT NOT NULL 
);



CREATE UNIQUE INDEX XPKProductos ON Productos
(id_producto   ASC);



ALTER TABLE Productos
	ADD CONSTRAINT  XPKProductos PRIMARY KEY (id_producto);



CREATE TABLE Productos_Pedidos
(
	id_producto          INTEGER NOT NULL ,
	id_pedido            INTEGER NOT NULL ,
	cantidad             INTEGER NOT NULL 
);



CREATE UNIQUE INDEX XPKProductos_Pedidos ON Productos_Pedidos
(id_producto   ASC,id_pedido   ASC);



ALTER TABLE Productos_Pedidos
	ADD CONSTRAINT  XPKProductos_Pedidos PRIMARY KEY (id_producto,id_pedido);



CREATE TABLE Puestos
(
	id_puesto            INTEGER NOT NULL ,
	salario              FLOAT NOT NULL ,
	nombre_puesto        VARCHAR2(40) NOT NULL 
);



CREATE UNIQUE INDEX XPKPuestos ON Puestos
(id_puesto   ASC);



ALTER TABLE Puestos
	ADD CONSTRAINT  XPKPuestos PRIMARY KEY (id_puesto);



CREATE TABLE Ref_Pago
(
	id_tarjeta           INTEGER NOT NULL ,
	num_tarjeta          VARCHAR2(16) NOT NULL ,
	seguridad            INTEGER NOT NULL ,
	tipo                 VARCHAR2(20) NOT NULL ,
	id_usuario           INTEGER NOT NULL ,
	mes                  INTEGER NOT NULL ,
	anio                 INTEGER NOT NULL 
);



CREATE UNIQUE INDEX XPKRef_Pago ON Ref_Pago
(id_tarjeta   ASC);



ALTER TABLE Ref_Pago
	ADD CONSTRAINT  XPKRef_Pago PRIMARY KEY (id_tarjeta);



CREATE TABLE Usuarios
(
	id_usuario           INTEGER NOT NULL ,
	contrasena           VARCHAR2(40) NOT NULL ,
	nombre               VARCHAR2(100) NOT NULL ,
	apellido             VARCHAR2(100) NOT NULL ,
	direccion            VARCHAR2(250) NOT NULL ,
	codigo_postal        INTEGER NOT NULL  CONSTRAINT  codigo_postal_valid_1110975354 CHECK (codigo_postal BETWEEN 0 AND 99999),
	pais                 VARCHAR2(50) NOT NULL ,
	estado               VARCHAR2(50) NOT NULL ,
	ciudad               VARCHAR2(50) NOT NULL ,
	detalles             VARCHAR2(250) DEFAULT  null  NULL ,
	usuario              VARCHAR2(40) NOT NULL ,
	email                VARCHAR2(150) NOT NULL 
);



CREATE UNIQUE INDEX XPKUsuarios ON Usuarios
(id_usuario   ASC);



ALTER TABLE Usuarios
	ADD CONSTRAINT  XPKUsuarios PRIMARY KEY (id_usuario);



ALTER TABLE Carrito
	ADD (CONSTRAINT R_58 FOREIGN KEY (id_usuario) REFERENCES Usuarios (id_usuario));



ALTER TABLE Carrito
	ADD (CONSTRAINT R_59 FOREIGN KEY (id_producto) REFERENCES Productos (id_producto));



ALTER TABLE Empleados
	ADD (CONSTRAINT R_10 FOREIGN KEY (id_bodega) REFERENCES Bodegas (id_bodega) ON DELETE SET NULL);



ALTER TABLE Empleados
	ADD (CONSTRAINT R_41 FOREIGN KEY (id_puesto) REFERENCES Puestos (id_puesto) ON DELETE SET NULL);



ALTER TABLE Favoritos
	ADD (CONSTRAINT R_11 FOREIGN KEY (id_usuario) REFERENCES Usuarios (id_usuario));



ALTER TABLE Favoritos
	ADD (CONSTRAINT R_12 FOREIGN KEY (id_producto) REFERENCES Productos (id_producto));



ALTER TABLE Galerias
	ADD (CONSTRAINT R_44 FOREIGN KEY (id_producto) REFERENCES Productos (id_producto));



ALTER TABLE Inventario
	ADD (CONSTRAINT R_20 FOREIGN KEY (id_producto) REFERENCES Productos (id_producto));



ALTER TABLE Inventario
	ADD (CONSTRAINT R_22 FOREIGN KEY (id_bodega) REFERENCES Bodegas (id_bodega));



ALTER TABLE Pedidos
	ADD (CONSTRAINT R_7 FOREIGN KEY (id_usuario) REFERENCES Usuarios (id_usuario) ON DELETE SET NULL);



ALTER TABLE Pedidos
	ADD (CONSTRAINT R_42 FOREIGN KEY (id_modalidad) REFERENCES Modalidades (id_modalidad) ON DELETE SET NULL);



ALTER TABLE Productos_Pedidos
	ADD (CONSTRAINT R_32 FOREIGN KEY (id_producto) REFERENCES Productos (id_producto));



ALTER TABLE Productos_Pedidos
	ADD (CONSTRAINT R_34 FOREIGN KEY (id_pedido) REFERENCES Pedidos (id_pedido));



ALTER TABLE Ref_Pago
	ADD (CONSTRAINT R_1 FOREIGN KEY (id_usuario) REFERENCES Usuarios (id_usuario) ON DELETE SET NULL);



CREATE  TRIGGER  tD_Bodegas AFTER DELETE ON Bodegas for each row
-- ERwin Builtin Trigger
-- DELETE trigger on Bodegas 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Bodegas  Empleados on parent delete set null */
    /* ERWIN_RELATION:CHECKSUM="0001a3b9", PARENT_OWNER="", PARENT_TABLE="Bodegas"
    CHILD_OWNER="", CHILD_TABLE="Empleados"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_10", FK_COLUMNS="id_bodega" */
    UPDATE Empleados
      SET
        /* %SetFK(Empleados,NULL) */
        Empleados.id_bodega = NULL
      WHERE
        /* %JoinFKPK(Empleados,:%Old," = "," AND") */
        Empleados.id_bodega = :old.id_bodega;

    /* ERwin Builtin Trigger */
    /* Bodegas  Inventario on parent delete restrict */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Bodegas"
    CHILD_OWNER="", CHILD_TABLE="Inventario"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_22", FK_COLUMNS="id_bodega" */
    SELECT count(*) INTO NUMROWS
      FROM Inventario
      WHERE
        /*  %JoinFKPK(Inventario,:%Old," = "," AND") */
        Inventario.id_bodega = :old.id_bodega;
    IF (NUMROWS > 0)
    THEN
      raise_application_error(
        -20001,
        'Cannot delete Bodegas because Inventario exists.'
      );
    END IF;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Bodegas AFTER UPDATE ON Bodegas for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Bodegas 
DECLARE NUMROWS INTEGER;
BEGIN
  /* Bodegas  Empleados on parent update set null */
  /* ERWIN_RELATION:CHECKSUM="0001ed98", PARENT_OWNER="", PARENT_TABLE="Bodegas"
    CHILD_OWNER="", CHILD_TABLE="Empleados"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_10", FK_COLUMNS="id_bodega" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_bodega <> :new.id_bodega
  THEN
    UPDATE Empleados
      SET
        /* %SetFK(Empleados,NULL) */
        Empleados.id_bodega = NULL
      WHERE
        /* %JoinFKPK(Empleados,:%Old," = ",",") */
        Empleados.id_bodega = :old.id_bodega;
  END IF;

  /* ERwin Builtin Trigger */
  /* Bodegas  Inventario on parent update restrict */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Bodegas"
    CHILD_OWNER="", CHILD_TABLE="Inventario"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_22", FK_COLUMNS="id_bodega" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_bodega <> :new.id_bodega
  THEN
    SELECT count(*) INTO NUMROWS
      FROM Inventario
      WHERE
        /*  %JoinFKPK(Inventario,:%Old," = "," AND") */
        Inventario.id_bodega = :old.id_bodega;
    IF (NUMROWS > 0)
    THEN 
      raise_application_error(
        -20005,
        'Cannot update Bodegas because Inventario exists.'
      );
    END IF;
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER tI_Carrito BEFORE INSERT ON Carrito for each row
-- ERwin Builtin Trigger
-- INSERT trigger on Carrito 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Usuarios  Carrito on child insert restrict */
    /* ERWIN_RELATION:CHECKSUM="0001eb45", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Carrito"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_58", FK_COLUMNS="id_usuario" */
    SELECT count(*) INTO NUMROWS
      FROM Usuarios
      WHERE
        /* %JoinFKPK(:%New,Usuarios," = "," AND") */
        :new.id_usuario = Usuarios.id_usuario;
    IF (
      /* %NotnullFK(:%New," IS NOT NULL AND") */
      
      NUMROWS = 0
    )
    THEN
      raise_application_error(
        -20002,
        'Cannot insert Carrito because Usuarios does not exist.'
      );
    END IF;

    /* ERwin Builtin Trigger */
    /* Productos  Carrito on child insert restrict */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Carrito"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_59", FK_COLUMNS="id_producto" */
    SELECT count(*) INTO NUMROWS
      FROM Productos
      WHERE
        /* %JoinFKPK(:%New,Productos," = "," AND") */
        :new.id_producto = Productos.id_producto;
    IF (
      /* %NotnullFK(:%New," IS NOT NULL AND") */
      
      NUMROWS = 0
    )
    THEN
      raise_application_error(
        -20002,
        'Cannot insert Carrito because Productos does not exist.'
      );
    END IF;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Carrito AFTER UPDATE ON Carrito for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Carrito 
DECLARE NUMROWS INTEGER;
BEGIN
  /* ERwin Builtin Trigger */
  /* Usuarios  Carrito on child update restrict */
  /* ERWIN_RELATION:CHECKSUM="0001e2d3", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Carrito"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_58", FK_COLUMNS="id_usuario" */
  SELECT count(*) INTO NUMROWS
    FROM Usuarios
    WHERE
      /* %JoinFKPK(:%New,Usuarios," = "," AND") */
      :new.id_usuario = Usuarios.id_usuario;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Carrito because Usuarios does not exist.'
    );
  END IF;

  /* ERwin Builtin Trigger */
  /* Productos  Carrito on child update restrict */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Carrito"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_59", FK_COLUMNS="id_producto" */
  SELECT count(*) INTO NUMROWS
    FROM Productos
    WHERE
      /* %JoinFKPK(:%New,Productos," = "," AND") */
      :new.id_producto = Productos.id_producto;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Carrito because Productos does not exist.'
    );
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER tI_Empleados BEFORE INSERT ON Empleados for each row
-- ERwin Builtin Trigger
-- INSERT trigger on Empleados 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Bodegas  Empleados on child insert set null */
    /* ERWIN_RELATION:CHECKSUM="0001ed8d", PARENT_OWNER="", PARENT_TABLE="Bodegas"
    CHILD_OWNER="", CHILD_TABLE="Empleados"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_10", FK_COLUMNS="id_bodega" */
    UPDATE Empleados
      SET
        /* %SetFK(Empleados,NULL) */
        Empleados.id_bodega = NULL
      WHERE
        NOT EXISTS (
          SELECT * FROM Bodegas
            WHERE
              /* %JoinFKPK(:%New,Bodegas," = "," AND") */
              :new.id_bodega = Bodegas.id_bodega
        ) 
        /* %JoinPKPK(Empleados,:%New," = "," AND") */
         and Empleados.id_empleado = :new.id_empleado;

    /* ERwin Builtin Trigger */
    /* Puestos  Empleados on child insert set null */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Puestos"
    CHILD_OWNER="", CHILD_TABLE="Empleados"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_41", FK_COLUMNS="id_puesto" */
    UPDATE Empleados
      SET
        /* %SetFK(Empleados,NULL) */
        Empleados.id_puesto = NULL
      WHERE
        NOT EXISTS (
          SELECT * FROM Puestos
            WHERE
              /* %JoinFKPK(:%New,Puestos," = "," AND") */
              :new.id_puesto = Puestos.id_puesto
        ) 
        /* %JoinPKPK(Empleados,:%New," = "," AND") */
         and Empleados.id_empleado = :new.id_empleado;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Empleados AFTER UPDATE ON Empleados for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Empleados 
DECLARE NUMROWS INTEGER;
BEGIN
  /* ERwin Builtin Trigger */
  /* Bodegas  Empleados on child update no action */
  /* ERWIN_RELATION:CHECKSUM="00020ac4", PARENT_OWNER="", PARENT_TABLE="Bodegas"
    CHILD_OWNER="", CHILD_TABLE="Empleados"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_10", FK_COLUMNS="id_bodega" */
  SELECT count(*) INTO NUMROWS
    FROM Bodegas
    WHERE
      /* %JoinFKPK(:%New,Bodegas," = "," AND") */
      :new.id_bodega = Bodegas.id_bodega;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    :new.id_bodega IS NOT NULL AND
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Empleados because Bodegas does not exist.'
    );
  END IF;

  /* ERwin Builtin Trigger */
  /* Puestos  Empleados on child update no action */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Puestos"
    CHILD_OWNER="", CHILD_TABLE="Empleados"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_41", FK_COLUMNS="id_puesto" */
  SELECT count(*) INTO NUMROWS
    FROM Puestos
    WHERE
      /* %JoinFKPK(:%New,Puestos," = "," AND") */
      :new.id_puesto = Puestos.id_puesto;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    :new.id_puesto IS NOT NULL AND
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Empleados because Puestos does not exist.'
    );
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER tI_Favoritos BEFORE INSERT ON Favoritos for each row
-- ERwin Builtin Trigger
-- INSERT trigger on Favoritos 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Usuarios  Favoritos on child insert restrict */
    /* ERWIN_RELATION:CHECKSUM="0001fad2", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Favoritos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_11", FK_COLUMNS="id_usuario" */
    SELECT count(*) INTO NUMROWS
      FROM Usuarios
      WHERE
        /* %JoinFKPK(:%New,Usuarios," = "," AND") */
        :new.id_usuario = Usuarios.id_usuario;
    IF (
      /* %NotnullFK(:%New," IS NOT NULL AND") */
      
      NUMROWS = 0
    )
    THEN
      raise_application_error(
        -20002,
        'Cannot insert Favoritos because Usuarios does not exist.'
      );
    END IF;

    /* ERwin Builtin Trigger */
    /* Productos  Favoritos on child insert restrict */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Favoritos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_12", FK_COLUMNS="id_producto" */
    SELECT count(*) INTO NUMROWS
      FROM Productos
      WHERE
        /* %JoinFKPK(:%New,Productos," = "," AND") */
        :new.id_producto = Productos.id_producto;
    IF (
      /* %NotnullFK(:%New," IS NOT NULL AND") */
      
      NUMROWS = 0
    )
    THEN
      raise_application_error(
        -20002,
        'Cannot insert Favoritos because Productos does not exist.'
      );
    END IF;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Favoritos AFTER UPDATE ON Favoritos for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Favoritos 
DECLARE NUMROWS INTEGER;
BEGIN
  /* ERwin Builtin Trigger */
  /* Usuarios  Favoritos on child update restrict */
  /* ERWIN_RELATION:CHECKSUM="000204f5", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Favoritos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_11", FK_COLUMNS="id_usuario" */
  SELECT count(*) INTO NUMROWS
    FROM Usuarios
    WHERE
      /* %JoinFKPK(:%New,Usuarios," = "," AND") */
      :new.id_usuario = Usuarios.id_usuario;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Favoritos because Usuarios does not exist.'
    );
  END IF;

  /* ERwin Builtin Trigger */
  /* Productos  Favoritos on child update restrict */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Favoritos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_12", FK_COLUMNS="id_producto" */
  SELECT count(*) INTO NUMROWS
    FROM Productos
    WHERE
      /* %JoinFKPK(:%New,Productos," = "," AND") */
      :new.id_producto = Productos.id_producto;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Favoritos because Productos does not exist.'
    );
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER tI_Galerias BEFORE INSERT ON Galerias for each row
-- ERwin Builtin Trigger
-- INSERT trigger on Galerias 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Productos  Galerias on child insert restrict */
    /* ERWIN_RELATION:CHECKSUM="0000e39c", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Galerias"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_44", FK_COLUMNS="id_producto" */
    SELECT count(*) INTO NUMROWS
      FROM Productos
      WHERE
        /* %JoinFKPK(:%New,Productos," = "," AND") */
        :new.id_producto = Productos.id_producto;
    IF (
      /* %NotnullFK(:%New," IS NOT NULL AND") */
      
      NUMROWS = 0
    )
    THEN
      raise_application_error(
        -20002,
        'Cannot insert Galerias because Productos does not exist.'
      );
    END IF;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Galerias AFTER UPDATE ON Galerias for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Galerias 
DECLARE NUMROWS INTEGER;
BEGIN
  /* ERwin Builtin Trigger */
  /* Productos  Galerias on child update restrict */
  /* ERWIN_RELATION:CHECKSUM="0000e3af", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Galerias"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_44", FK_COLUMNS="id_producto" */
  SELECT count(*) INTO NUMROWS
    FROM Productos
    WHERE
      /* %JoinFKPK(:%New,Productos," = "," AND") */
      :new.id_producto = Productos.id_producto;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Galerias because Productos does not exist.'
    );
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER tI_Inventario BEFORE INSERT ON Inventario for each row
-- ERwin Builtin Trigger
-- INSERT trigger on Inventario 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Productos  Inventario on child insert restrict */
    /* ERWIN_RELATION:CHECKSUM="0001fb9c", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Inventario"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_20", FK_COLUMNS="id_producto" */
    SELECT count(*) INTO NUMROWS
      FROM Productos
      WHERE
        /* %JoinFKPK(:%New,Productos," = "," AND") */
        :new.id_producto = Productos.id_producto;
    IF (
      /* %NotnullFK(:%New," IS NOT NULL AND") */
      
      NUMROWS = 0
    )
    THEN
      raise_application_error(
        -20002,
        'Cannot insert Inventario because Productos does not exist.'
      );
    END IF;

    /* ERwin Builtin Trigger */
    /* Bodegas  Inventario on child insert restrict */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Bodegas"
    CHILD_OWNER="", CHILD_TABLE="Inventario"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_22", FK_COLUMNS="id_bodega" */
    SELECT count(*) INTO NUMROWS
      FROM Bodegas
      WHERE
        /* %JoinFKPK(:%New,Bodegas," = "," AND") */
        :new.id_bodega = Bodegas.id_bodega;
    IF (
      /* %NotnullFK(:%New," IS NOT NULL AND") */
      
      NUMROWS = 0
    )
    THEN
      raise_application_error(
        -20002,
        'Cannot insert Inventario because Bodegas does not exist.'
      );
    END IF;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Inventario AFTER UPDATE ON Inventario for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Inventario 
DECLARE NUMROWS INTEGER;
BEGIN
  /* ERwin Builtin Trigger */
  /* Productos  Inventario on child update restrict */
  /* ERWIN_RELATION:CHECKSUM="0001fdc3", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Inventario"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_20", FK_COLUMNS="id_producto" */
  SELECT count(*) INTO NUMROWS
    FROM Productos
    WHERE
      /* %JoinFKPK(:%New,Productos," = "," AND") */
      :new.id_producto = Productos.id_producto;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Inventario because Productos does not exist.'
    );
  END IF;

  /* ERwin Builtin Trigger */
  /* Bodegas  Inventario on child update restrict */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Bodegas"
    CHILD_OWNER="", CHILD_TABLE="Inventario"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_22", FK_COLUMNS="id_bodega" */
  SELECT count(*) INTO NUMROWS
    FROM Bodegas
    WHERE
      /* %JoinFKPK(:%New,Bodegas," = "," AND") */
      :new.id_bodega = Bodegas.id_bodega;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Inventario because Bodegas does not exist.'
    );
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER  tD_Modalidades AFTER DELETE ON Modalidades for each row
-- ERwin Builtin Trigger
-- DELETE trigger on Modalidades 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Modalidades  Pedidos on parent delete set null */
    /* ERWIN_RELATION:CHECKSUM="0000c170", PARENT_OWNER="", PARENT_TABLE="Modalidades"
    CHILD_OWNER="", CHILD_TABLE="Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_42", FK_COLUMNS="id_modalidad" */
    UPDATE Pedidos
      SET
        /* %SetFK(Pedidos,NULL) */
        Pedidos.id_modalidad = NULL
      WHERE
        /* %JoinFKPK(Pedidos,:%Old," = "," AND") */
        Pedidos.id_modalidad = :old.id_modalidad;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Modalidades AFTER UPDATE ON Modalidades for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Modalidades 
DECLARE NUMROWS INTEGER;
BEGIN
  /* Modalidades  Pedidos on parent update set null */
  /* ERWIN_RELATION:CHECKSUM="0000e89a", PARENT_OWNER="", PARENT_TABLE="Modalidades"
    CHILD_OWNER="", CHILD_TABLE="Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_42", FK_COLUMNS="id_modalidad" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_modalidad <> :new.id_modalidad
  THEN
    UPDATE Pedidos
      SET
        /* %SetFK(Pedidos,NULL) */
        Pedidos.id_modalidad = NULL
      WHERE
        /* %JoinFKPK(Pedidos,:%Old," = ",",") */
        Pedidos.id_modalidad = :old.id_modalidad;
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER tI_Pedidos BEFORE INSERT ON Pedidos for each row
-- ERwin Builtin Trigger
-- INSERT trigger on Pedidos 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Usuarios  Pedidos on child insert set null */
    /* ERWIN_RELATION:CHECKSUM="0001eef5", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_7", FK_COLUMNS="id_usuario" */
    UPDATE Pedidos
      SET
        /* %SetFK(Pedidos,NULL) */
        Pedidos.id_usuario = NULL
      WHERE
        NOT EXISTS (
          SELECT * FROM Usuarios
            WHERE
              /* %JoinFKPK(:%New,Usuarios," = "," AND") */
              :new.id_usuario = Usuarios.id_usuario
        ) 
        /* %JoinPKPK(Pedidos,:%New," = "," AND") */
         and Pedidos.id_pedido = :new.id_pedido;

    /* ERwin Builtin Trigger */
    /* Modalidades  Pedidos on child insert set null */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Modalidades"
    CHILD_OWNER="", CHILD_TABLE="Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_42", FK_COLUMNS="id_modalidad" */
    UPDATE Pedidos
      SET
        /* %SetFK(Pedidos,NULL) */
        Pedidos.id_modalidad = NULL
      WHERE
        NOT EXISTS (
          SELECT * FROM Modalidades
            WHERE
              /* %JoinFKPK(:%New,Modalidades," = "," AND") */
              :new.id_modalidad = Modalidades.id_modalidad
        ) 
        /* %JoinPKPK(Pedidos,:%New," = "," AND") */
         and Pedidos.id_pedido = :new.id_pedido;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER  tD_Pedidos AFTER DELETE ON Pedidos for each row
-- ERwin Builtin Trigger
-- DELETE trigger on Pedidos 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Pedidos  Productos_Pedidos on parent delete restrict */
    /* ERWIN_RELATION:CHECKSUM="0000ecbf", PARENT_OWNER="", PARENT_TABLE="Pedidos"
    CHILD_OWNER="", CHILD_TABLE="Productos_Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_34", FK_COLUMNS="id_pedido" */
    SELECT count(*) INTO NUMROWS
      FROM Productos_Pedidos
      WHERE
        /*  %JoinFKPK(Productos_Pedidos,:%Old," = "," AND") */
        Productos_Pedidos.id_pedido = :old.id_pedido;
    IF (NUMROWS > 0)
    THEN
      raise_application_error(
        -20001,
        'Cannot delete Pedidos because Productos_Pedidos exists.'
      );
    END IF;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Pedidos AFTER UPDATE ON Pedidos for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Pedidos 
DECLARE NUMROWS INTEGER;
BEGIN
  /* ERwin Builtin Trigger */
  /* Pedidos  Productos_Pedidos on parent update restrict */
  /* ERWIN_RELATION:CHECKSUM="00034173", PARENT_OWNER="", PARENT_TABLE="Pedidos"
    CHILD_OWNER="", CHILD_TABLE="Productos_Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_34", FK_COLUMNS="id_pedido" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_pedido <> :new.id_pedido
  THEN
    SELECT count(*) INTO NUMROWS
      FROM Productos_Pedidos
      WHERE
        /*  %JoinFKPK(Productos_Pedidos,:%Old," = "," AND") */
        Productos_Pedidos.id_pedido = :old.id_pedido;
    IF (NUMROWS > 0)
    THEN 
      raise_application_error(
        -20005,
        'Cannot update Pedidos because Productos_Pedidos exists.'
      );
    END IF;
  END IF;

  /* ERwin Builtin Trigger */
  /* Usuarios  Pedidos on child update no action */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_7", FK_COLUMNS="id_usuario" */
  SELECT count(*) INTO NUMROWS
    FROM Usuarios
    WHERE
      /* %JoinFKPK(:%New,Usuarios," = "," AND") */
      :new.id_usuario = Usuarios.id_usuario;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    :new.id_usuario IS NOT NULL AND
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Pedidos because Usuarios does not exist.'
    );
  END IF;

  /* ERwin Builtin Trigger */
  /* Modalidades  Pedidos on child update no action */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Modalidades"
    CHILD_OWNER="", CHILD_TABLE="Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_42", FK_COLUMNS="id_modalidad" */
  SELECT count(*) INTO NUMROWS
    FROM Modalidades
    WHERE
      /* %JoinFKPK(:%New,Modalidades," = "," AND") */
      :new.id_modalidad = Modalidades.id_modalidad;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    :new.id_modalidad IS NOT NULL AND
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Pedidos because Modalidades does not exist.'
    );
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER  tD_Productos AFTER DELETE ON Productos for each row
-- ERwin Builtin Trigger
-- DELETE trigger on Productos 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Productos  Favoritos on parent delete restrict */
    /* ERWIN_RELATION:CHECKSUM="0004c565", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Favoritos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_12", FK_COLUMNS="id_producto" */
    SELECT count(*) INTO NUMROWS
      FROM Favoritos
      WHERE
        /*  %JoinFKPK(Favoritos,:%Old," = "," AND") */
        Favoritos.id_producto = :old.id_producto;
    IF (NUMROWS > 0)
    THEN
      raise_application_error(
        -20001,
        'Cannot delete Productos because Favoritos exists.'
      );
    END IF;

    /* ERwin Builtin Trigger */
    /* Productos  Galerias on parent delete restrict */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Galerias"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_44", FK_COLUMNS="id_producto" */
    SELECT count(*) INTO NUMROWS
      FROM Galerias
      WHERE
        /*  %JoinFKPK(Galerias,:%Old," = "," AND") */
        Galerias.id_producto = :old.id_producto;
    IF (NUMROWS > 0)
    THEN
      raise_application_error(
        -20001,
        'Cannot delete Productos because Galerias exists.'
      );
    END IF;

    /* ERwin Builtin Trigger */
    /* Productos  Productos_Pedidos on parent delete restrict */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Productos_Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_32", FK_COLUMNS="id_producto" */
    SELECT count(*) INTO NUMROWS
      FROM Productos_Pedidos
      WHERE
        /*  %JoinFKPK(Productos_Pedidos,:%Old," = "," AND") */
        Productos_Pedidos.id_producto = :old.id_producto;
    IF (NUMROWS > 0)
    THEN
      raise_application_error(
        -20001,
        'Cannot delete Productos because Productos_Pedidos exists.'
      );
    END IF;

    /* ERwin Builtin Trigger */
    /* Productos  Inventario on parent delete restrict */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Inventario"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_20", FK_COLUMNS="id_producto" */
    SELECT count(*) INTO NUMROWS
      FROM Inventario
      WHERE
        /*  %JoinFKPK(Inventario,:%Old," = "," AND") */
        Inventario.id_producto = :old.id_producto;
    IF (NUMROWS > 0)
    THEN
      raise_application_error(
        -20001,
        'Cannot delete Productos because Inventario exists.'
      );
    END IF;

    /* ERwin Builtin Trigger */
    /* Productos  Carrito on parent delete restrict */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Carrito"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_59", FK_COLUMNS="id_producto" */
    SELECT count(*) INTO NUMROWS
      FROM Carrito
      WHERE
        /*  %JoinFKPK(Carrito,:%Old," = "," AND") */
        Carrito.id_producto = :old.id_producto;
    IF (NUMROWS > 0)
    THEN
      raise_application_error(
        -20001,
        'Cannot delete Productos because Carrito exists.'
      );
    END IF;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Productos AFTER UPDATE ON Productos for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Productos 
DECLARE NUMROWS INTEGER;
BEGIN
  /* ERwin Builtin Trigger */
  /* Productos  Favoritos on parent update restrict */
  /* ERWIN_RELATION:CHECKSUM="0005895f", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Favoritos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_12", FK_COLUMNS="id_producto" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_producto <> :new.id_producto
  THEN
    SELECT count(*) INTO NUMROWS
      FROM Favoritos
      WHERE
        /*  %JoinFKPK(Favoritos,:%Old," = "," AND") */
        Favoritos.id_producto = :old.id_producto;
    IF (NUMROWS > 0)
    THEN 
      raise_application_error(
        -20005,
        'Cannot update Productos because Favoritos exists.'
      );
    END IF;
  END IF;

  /* ERwin Builtin Trigger */
  /* Productos  Galerias on parent update restrict */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Galerias"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_44", FK_COLUMNS="id_producto" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_producto <> :new.id_producto
  THEN
    SELECT count(*) INTO NUMROWS
      FROM Galerias
      WHERE
        /*  %JoinFKPK(Galerias,:%Old," = "," AND") */
        Galerias.id_producto = :old.id_producto;
    IF (NUMROWS > 0)
    THEN 
      raise_application_error(
        -20005,
        'Cannot update Productos because Galerias exists.'
      );
    END IF;
  END IF;

  /* ERwin Builtin Trigger */
  /* Productos  Productos_Pedidos on parent update restrict */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Productos_Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_32", FK_COLUMNS="id_producto" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_producto <> :new.id_producto
  THEN
    SELECT count(*) INTO NUMROWS
      FROM Productos_Pedidos
      WHERE
        /*  %JoinFKPK(Productos_Pedidos,:%Old," = "," AND") */
        Productos_Pedidos.id_producto = :old.id_producto;
    IF (NUMROWS > 0)
    THEN 
      raise_application_error(
        -20005,
        'Cannot update Productos because Productos_Pedidos exists.'
      );
    END IF;
  END IF;

  /* ERwin Builtin Trigger */
  /* Productos  Inventario on parent update restrict */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Inventario"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_20", FK_COLUMNS="id_producto" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_producto <> :new.id_producto
  THEN
    SELECT count(*) INTO NUMROWS
      FROM Inventario
      WHERE
        /*  %JoinFKPK(Inventario,:%Old," = "," AND") */
        Inventario.id_producto = :old.id_producto;
    IF (NUMROWS > 0)
    THEN 
      raise_application_error(
        -20005,
        'Cannot update Productos because Inventario exists.'
      );
    END IF;
  END IF;

  /* ERwin Builtin Trigger */
  /* Productos  Carrito on parent update restrict */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Carrito"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_59", FK_COLUMNS="id_producto" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_producto <> :new.id_producto
  THEN
    SELECT count(*) INTO NUMROWS
      FROM Carrito
      WHERE
        /*  %JoinFKPK(Carrito,:%Old," = "," AND") */
        Carrito.id_producto = :old.id_producto;
    IF (NUMROWS > 0)
    THEN 
      raise_application_error(
        -20005,
        'Cannot update Productos because Carrito exists.'
      );
    END IF;
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER tI_Productos_Pedidos BEFORE INSERT ON Productos_Pedidos for each row
-- ERwin Builtin Trigger
-- INSERT trigger on Productos_Pedidos 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Productos  Productos_Pedidos on child insert restrict */
    /* ERWIN_RELATION:CHECKSUM="000200cd", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Productos_Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_32", FK_COLUMNS="id_producto" */
    SELECT count(*) INTO NUMROWS
      FROM Productos
      WHERE
        /* %JoinFKPK(:%New,Productos," = "," AND") */
        :new.id_producto = Productos.id_producto;
    IF (
      /* %NotnullFK(:%New," IS NOT NULL AND") */
      
      NUMROWS = 0
    )
    THEN
      raise_application_error(
        -20002,
        'Cannot insert Productos_Pedidos because Productos does not exist.'
      );
    END IF;

    /* ERwin Builtin Trigger */
    /* Pedidos  Productos_Pedidos on child insert restrict */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Pedidos"
    CHILD_OWNER="", CHILD_TABLE="Productos_Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_34", FK_COLUMNS="id_pedido" */
    SELECT count(*) INTO NUMROWS
      FROM Pedidos
      WHERE
        /* %JoinFKPK(:%New,Pedidos," = "," AND") */
        :new.id_pedido = Pedidos.id_pedido;
    IF (
      /* %NotnullFK(:%New," IS NOT NULL AND") */
      
      NUMROWS = 0
    )
    THEN
      raise_application_error(
        -20002,
        'Cannot insert Productos_Pedidos because Pedidos does not exist.'
      );
    END IF;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Productos_Pedidos AFTER UPDATE ON Productos_Pedidos for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Productos_Pedidos 
DECLARE NUMROWS INTEGER;
BEGIN
  /* ERwin Builtin Trigger */
  /* Productos  Productos_Pedidos on child update restrict */
  /* ERWIN_RELATION:CHECKSUM="0001f9d8", PARENT_OWNER="", PARENT_TABLE="Productos"
    CHILD_OWNER="", CHILD_TABLE="Productos_Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_32", FK_COLUMNS="id_producto" */
  SELECT count(*) INTO NUMROWS
    FROM Productos
    WHERE
      /* %JoinFKPK(:%New,Productos," = "," AND") */
      :new.id_producto = Productos.id_producto;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Productos_Pedidos because Productos does not exist.'
    );
  END IF;

  /* ERwin Builtin Trigger */
  /* Pedidos  Productos_Pedidos on child update restrict */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Pedidos"
    CHILD_OWNER="", CHILD_TABLE="Productos_Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_34", FK_COLUMNS="id_pedido" */
  SELECT count(*) INTO NUMROWS
    FROM Pedidos
    WHERE
      /* %JoinFKPK(:%New,Pedidos," = "," AND") */
      :new.id_pedido = Pedidos.id_pedido;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Productos_Pedidos because Pedidos does not exist.'
    );
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER  tD_Puestos AFTER DELETE ON Puestos for each row
-- ERwin Builtin Trigger
-- DELETE trigger on Puestos 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Puestos  Empleados on parent delete set null */
    /* ERWIN_RELATION:CHECKSUM="0000ae5e", PARENT_OWNER="", PARENT_TABLE="Puestos"
    CHILD_OWNER="", CHILD_TABLE="Empleados"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_41", FK_COLUMNS="id_puesto" */
    UPDATE Empleados
      SET
        /* %SetFK(Empleados,NULL) */
        Empleados.id_puesto = NULL
      WHERE
        /* %JoinFKPK(Empleados,:%Old," = "," AND") */
        Empleados.id_puesto = :old.id_puesto;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Puestos AFTER UPDATE ON Puestos for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Puestos 
DECLARE NUMROWS INTEGER;
BEGIN
  /* Puestos  Empleados on parent update set null */
  /* ERWIN_RELATION:CHECKSUM="0000db2a", PARENT_OWNER="", PARENT_TABLE="Puestos"
    CHILD_OWNER="", CHILD_TABLE="Empleados"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_41", FK_COLUMNS="id_puesto" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_puesto <> :new.id_puesto
  THEN
    UPDATE Empleados
      SET
        /* %SetFK(Empleados,NULL) */
        Empleados.id_puesto = NULL
      WHERE
        /* %JoinFKPK(Empleados,:%Old," = ",",") */
        Empleados.id_puesto = :old.id_puesto;
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER tI_Ref_Pago BEFORE INSERT ON Ref_Pago for each row
-- ERwin Builtin Trigger
-- INSERT trigger on Ref_Pago 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Usuarios  Ref_Pago on child insert set null */
    /* ERWIN_RELATION:CHECKSUM="0000edae", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Ref_Pago"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="id_usuario" */
    UPDATE Ref_Pago
      SET
        /* %SetFK(Ref_Pago,NULL) */
        Ref_Pago.id_usuario = NULL
      WHERE
        NOT EXISTS (
          SELECT * FROM Usuarios
            WHERE
              /* %JoinFKPK(:%New,Usuarios," = "," AND") */
              :new.id_usuario = Usuarios.id_usuario
        ) 
        /* %JoinPKPK(Ref_Pago,:%New," = "," AND") */
         and Ref_Pago.id_tarjeta = :new.id_tarjeta;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Ref_Pago AFTER UPDATE ON Ref_Pago for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Ref_Pago 
DECLARE NUMROWS INTEGER;
BEGIN
  /* ERwin Builtin Trigger */
  /* Usuarios  Ref_Pago on child update no action */
  /* ERWIN_RELATION:CHECKSUM="0000f996", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Ref_Pago"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="id_usuario" */
  SELECT count(*) INTO NUMROWS
    FROM Usuarios
    WHERE
      /* %JoinFKPK(:%New,Usuarios," = "," AND") */
      :new.id_usuario = Usuarios.id_usuario;
  IF (
    /* %NotnullFK(:%New," IS NOT NULL AND") */
    :new.id_usuario IS NOT NULL AND
    NUMROWS = 0
  )
  THEN
    raise_application_error(
      -20007,
      'Cannot update Ref_Pago because Usuarios does not exist.'
    );
  END IF;


-- ERwin Builtin Trigger
END;
/


CREATE  TRIGGER  tD_Usuarios AFTER DELETE ON Usuarios for each row
-- ERwin Builtin Trigger
-- DELETE trigger on Usuarios 
DECLARE NUMROWS INTEGER;
BEGIN
    /* ERwin Builtin Trigger */
    /* Usuarios  Ref_Pago on parent delete set null */
    /* ERWIN_RELATION:CHECKSUM="00034dee", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Ref_Pago"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="id_usuario" */
    UPDATE Ref_Pago
      SET
        /* %SetFK(Ref_Pago,NULL) */
        Ref_Pago.id_usuario = NULL
      WHERE
        /* %JoinFKPK(Ref_Pago,:%Old," = "," AND") */
        Ref_Pago.id_usuario = :old.id_usuario;

    /* ERwin Builtin Trigger */
    /* Usuarios  Pedidos on parent delete set null */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_7", FK_COLUMNS="id_usuario" */
    UPDATE Pedidos
      SET
        /* %SetFK(Pedidos,NULL) */
        Pedidos.id_usuario = NULL
      WHERE
        /* %JoinFKPK(Pedidos,:%Old," = "," AND") */
        Pedidos.id_usuario = :old.id_usuario;

    /* ERwin Builtin Trigger */
    /* Usuarios  Favoritos on parent delete restrict */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Favoritos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_11", FK_COLUMNS="id_usuario" */
    SELECT count(*) INTO NUMROWS
      FROM Favoritos
      WHERE
        /*  %JoinFKPK(Favoritos,:%Old," = "," AND") */
        Favoritos.id_usuario = :old.id_usuario;
    IF (NUMROWS > 0)
    THEN
      raise_application_error(
        -20001,
        'Cannot delete Usuarios because Favoritos exists.'
      );
    END IF;

    /* ERwin Builtin Trigger */
    /* Usuarios  Carrito on parent delete restrict */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Carrito"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_58", FK_COLUMNS="id_usuario" */
    SELECT count(*) INTO NUMROWS
      FROM Carrito
      WHERE
        /*  %JoinFKPK(Carrito,:%Old," = "," AND") */
        Carrito.id_usuario = :old.id_usuario;
    IF (NUMROWS > 0)
    THEN
      raise_application_error(
        -20001,
        'Cannot delete Usuarios because Carrito exists.'
      );
    END IF;


-- ERwin Builtin Trigger
END;
/

CREATE  TRIGGER tU_Usuarios AFTER UPDATE ON Usuarios for each row
-- ERwin Builtin Trigger
-- UPDATE trigger on Usuarios 
DECLARE NUMROWS INTEGER;
BEGIN
  /* Usuarios  Ref_Pago on parent update set null */
  /* ERWIN_RELATION:CHECKSUM="0003f583", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Ref_Pago"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="id_usuario" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_usuario <> :new.id_usuario
  THEN
    UPDATE Ref_Pago
      SET
        /* %SetFK(Ref_Pago,NULL) */
        Ref_Pago.id_usuario = NULL
      WHERE
        /* %JoinFKPK(Ref_Pago,:%Old," = ",",") */
        Ref_Pago.id_usuario = :old.id_usuario;
  END IF;

  /* Usuarios  Pedidos on parent update set null */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Pedidos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_7", FK_COLUMNS="id_usuario" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_usuario <> :new.id_usuario
  THEN
    UPDATE Pedidos
      SET
        /* %SetFK(Pedidos,NULL) */
        Pedidos.id_usuario = NULL
      WHERE
        /* %JoinFKPK(Pedidos,:%Old," = ",",") */
        Pedidos.id_usuario = :old.id_usuario;
  END IF;

  /* ERwin Builtin Trigger */
  /* Usuarios  Favoritos on parent update restrict */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Favoritos"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_11", FK_COLUMNS="id_usuario" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_usuario <> :new.id_usuario
  THEN
    SELECT count(*) INTO NUMROWS
      FROM Favoritos
      WHERE
        /*  %JoinFKPK(Favoritos,:%Old," = "," AND") */
        Favoritos.id_usuario = :old.id_usuario;
    IF (NUMROWS > 0)
    THEN 
      raise_application_error(
        -20005,
        'Cannot update Usuarios because Favoritos exists.'
      );
    END IF;
  END IF;

  /* ERwin Builtin Trigger */
  /* Usuarios  Carrito on parent update restrict */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Usuarios"
    CHILD_OWNER="", CHILD_TABLE="Carrito"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_58", FK_COLUMNS="id_usuario" */
  IF
    /* %JoinPKPK(:%Old,:%New," <> "," OR ") */
    :old.id_usuario <> :new.id_usuario
  THEN
    SELECT count(*) INTO NUMROWS
      FROM Carrito
      WHERE
        /*  %JoinFKPK(Carrito,:%Old," = "," AND") */
        Carrito.id_usuario = :old.id_usuario;
    IF (NUMROWS > 0)
    THEN 
      raise_application_error(
        -20005,
        'Cannot update Usuarios because Carrito exists.'
      );
    END IF;
  END IF;


-- ERwin Builtin Trigger
END;
/

