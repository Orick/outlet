<?php

require_once("inc/conexion.php");

session_start();

$contrasenia = trim(filter_input(INPUT_POST, 'contrasenia', FILTER_SANITIZE_STRING));
$nombre = trim(filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING));
$apellido = trim(filter_input(INPUT_POST, 'apellidos', FILTER_SANITIZE_STRING));
$direccion = trim(filter_input(INPUT_POST, 'direccion', FILTER_SANITIZE_STRING));
$zip = trim(filter_input(INPUT_POST, 'zip', FILTER_SANITIZE_STRING));
$pais = trim(filter_input(INPUT_POST, 'pais', FILTER_SANITIZE_STRING));
$estado = trim(filter_input(INPUT_POST, 'estado', FILTER_SANITIZE_STRING));
$ciudad = trim(filter_input(INPUT_POST, 'ciudad', FILTER_SANITIZE_STRING));

$confirmar = trim(filter_input(INPUT_POST, 'confirmar', FILTER_SANITIZE_STRING));

if( $nombre == "" || $apellido == "" || $direccion == ""
  || $zip == "" || $pais == "" || $estado  == "" || $ciudad == "" || !isset($_POST['confirmar'])){
    header("Location:perfil.php?status=error02");
    die;
}

if(!is_numeric($zip)){
  header("Location:perfil.php?status=error02");
  exit;
} else if($zip > 99999){
  header("Location:perfil.php?status=error02");
  exit;
}

$consulta = "SELECT contrasena FROM usuarios WHERE id_usuario = " . $_SESSION['id'];
$query = $conn->prepare($consulta);
$query->execute();

while($row = $query->fetch(PDO::FETCH_ASSOC)){
  if($confirmar !== $row['CONTRASENA']){
    header("Location:perfil.php?status=error03");
    die;
  }
}

if(strlen($contrasenia) != 0){
  $sql = "UPDATE usuarios SET ".
         "contrasena = '$contrasenia', " .
         "nombre = '$nombre', " .
         "apellido = '$apellido', " .
         "direccion = '$direccion', " .
         "codigo_postal = $zip, " .
         "pais = '$pais', " .
         "estado = '$estado', " .
         "ciudad = '$ciudad' " .
         "WHERE id_usuario = " . $_SESSION['id'];
} else {
  $sql = "UPDATE usuarios SET ".
         "nombre = '$nombre', " .
         "apellido = '$apellido', " .
         "direccion = '$direccion', " .
         "codigo_postal = $zip, " .
         "pais = '$pais', " .
         "estado = '$estado', " .
         "ciudad = '$ciudad' " .
         "WHERE id_usuario = " . $_SESSION['id'];
}

if(!isset($sql)){
  header("Location:perfil.php?status=error02");
  exit;
}

$query = $conn->prepare($sql);
$query->execute();

header("Location:perfil.php?status=actualizado");

?>
