<?php
 include("inc/header.php");
 include_once("inc/conexion.php");
?>

        <!--Aqui va el slider-->
        <div id="carouselPrincipal" class="carousel slide mb-3" data-ride="carousel">
          <div class="carousel-inner">
          <?php
            //Código para mostrar el slider desde las imagenes de la base de datos
            $sql = "SELECT direccion_imagen, slider, nombre_producto, precio, p.id_producto FROM galerias g";
            $sql .= " INNER JOIN productos p";
            $sql .= " ON g.id_producto = p.id_producto";
            $sql .= " WHERE ROWNUM <= 5 AND SLIDER = 1 ORDER BY id_imagen DESC";
            $query = $conn->prepare($sql);
            $query->execute();
            $band = true;
            $num = 0;
            while($row = $query->fetch(PDO::FETCH_ASSOC)){
          ?>
            <div class="carousel-item <?php if($band){$band = false; echo 'active';} ?>">
              <img src="<?php echo 'img/' . $row['DIRECCION_IMAGEN']; ?>">
              <div class="carousel-caption d-none d-md-block">
                <h4 class="mt-2">
                  <strong><?php echo $row['NOMBRE_PRODUCTO'] ?></strong> | <?php echo $row['PRECIO'] . "MXN" ?>
                </h4>
                <p><a href="articulo.php?id=<?php echo $row['ID_PRODUCTO'] ?>">
                  <button type="button" class="btn btn-success ml-1">Comprar</button></a>
                </p>
              </div>
            </div>
          <?php $num++;} ?>
            <!--Parte final del carrousel-->
            <ol class="carousel-indicators">
              <?php for($i = 0; $i < $num; $i++){ ?>
              <li data-target="#carouselPrincipal" data-slide-to="<?php echo $i; ?>"
                class="<?php if($i == 0){echo 'active';} ?>"></li>
            <?php } ?>
            </ol>
          <a class="carousel-control-prev" href="#carouselPrincipal" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselPrincipal" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        <!-- Fin del carrusel-->
        <div class="text-left">
          <h4>Catálago</h4>
          <hr>
          <div class="card-columns">
            <?php
              // Mostrar los últimos 8 articulos
              $sql = "SELECT direccion_imagen, slider, nombre_producto, precio, p.id_producto FROM galerias g";
              $sql .= " INNER JOIN productos p";
              $sql .= " ON g.id_producto = p.id_producto";
              $sql .= " WHERE ROWNUM <= 9 AND SLIDER = 0 ORDER BY id_imagen DESC";
              $query = $conn->prepare($sql);
              $query->execute();
              while($row = $query->fetch(PDO::FETCH_ASSOC)){
            ?>
            <div class="card border-dark">
              <img class="card-img-top" src="<?php echo "img/".$row['DIRECCION_IMAGEN']; ?>">
              <div class="card-body">
                <h4 class="card-title"><?php echo $row['NOMBRE_PRODUCTO']; ?></h4>
                <div class="mt-4">
                  <p><?php echo $row['PRECIO']."MXN"; ?>
                    <a href="articulo.php?id=<?php echo $row['ID_PRODUCTO']; ?>">
                      <button type="button" class="btn-sm btn-success float-right">Comprar</button>
                    </a>
                  </p>
                </div>
              </div>
            </div>
          <?php } ?>
          </div>
        </div>
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <?php
    include("inc/jquery.php");
    include("inc/footer.php");
    ?>
