<?php
  session_start();
  if(!isset($_SESSION['id'])){
    header("Location:index.php");
  }
  include("inc/conexion.php");

  //Quitar del carrito
  if(isset($_POST['descarrito'])){
    $sql = "DELETE FROM carrito WHERE id_usuario = ".$_SESSION['id']." AND id_producto = ".$_POST['descarrito'];
    $query = $conn->prepare($sql);
    $query->execute();
  }

  include("inc/header.php");

  $sql = "SELECT p.id_producto, nombre_producto, cantidad, precio FROM productos p";
  $sql .= " INNER JOIN carrito c";
  $sql .= " ON p.id_producto = c.id_producto";
  $sql .= " WHERE id_usuario = ".$_SESSION['id'];
  $query = $conn->prepare($sql);
  $query->execute();
  $i = 0;
  $pagar = 0;
?>

<div class="text-right  ">
  <form action="comprar.php" method="post">
    <table class="table">
      <thead>
        <tr>
          <th scope="col"></th>
          <th scope="col">Nombre del producto</th>
          <th scope="col">Precio</th>
          <th scope="col">Cantidad</th>
          <th scope="col">Total</th>
        </tr>
      </thead>
      <tbody>
        <?php
          while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $id = $row['ID_PRODUCTO'];
            $nombre = $row['NOMBRE_PRODUCTO'];
            $precio = $row['PRECIO'];
            $cantidad = $row['CANTIDAD'];
            $total = $precio * $cantidad;
            $pagar += $total;
            $i++;
        ?>
        <tr class="text-right">
          <form action="carrito.php" method="post">
            <td><button class="badge badge-danger" name="descarrito" value="<?php echo $id; ?>">x</button></td>
          </form>
          <td><?php echo $nombre; ?></td>
          <td><?php echo $precio. " MXN"; ?></td>
          <td><input type="number" class="form-control text-right" name="<?php echo $id?>" value="<?php echo $cantidad ?>"
            min="1" required></td>
          <td><?php echo $total. " MXN" ?></td>
          <?php } ?>
        </tr>
      </tbody>
    </table>
    <h5>Total: <?php echo $pagar; ?> MXN</h5><br>
    <button type="submit" class="btn btn-success">Siguiente paso</button>
  </form>
</div>

<?php
  include("inc/jquery.php");
  include("inc/footer.php");
?>
