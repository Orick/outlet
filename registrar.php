<?php

require_once("inc/conexion.php");

var_dump($_POST);

$usuario = trim(filter_input(INPUT_POST, 'usuario', FILTER_SANITIZE_STRING));
$contrasenia = trim(filter_input(INPUT_POST, 'contrasenia', FILTER_SANITIZE_STRING));
$email = trim(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL));
$nombre = trim(filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING));
$apellido = trim(filter_input(INPUT_POST, 'apellidos', FILTER_SANITIZE_STRING));
$direccion = trim(filter_input(INPUT_POST, 'direccion', FILTER_SANITIZE_STRING));
$zip = trim(filter_input(INPUT_POST, 'zip', FILTER_SANITIZE_STRING));
$pais = trim(filter_input(INPUT_POST, 'pais', FILTER_SANITIZE_STRING));
$estado = trim(filter_input(INPUT_POST, 'estado', FILTER_SANITIZE_STRING));
$ciudad = trim(filter_input(INPUT_POST, 'ciudad', FILTER_SANITIZE_STRING));

if($usuario == "" || $contrasenia == "" || $nombre == "" || $apellido == "" || $direccion == ""
  || $zip == "" || $pais == "" || $estado  == "" || $ciudad == "" || !isset($_POST['usuario'])){
    header("Location:index.php?status=error");
    exit;
}

if(!is_numeric($zip)){
  header("Location:index.php?status=error");
  exit;
}else if($zip > 99999){
  header("Location:index.php?status=error");
  exit;
}

$sql = "SELECT usuario FROM usuarios";
$query = $conn->prepare($sql);
$query->execute();

while($row = $query->fetch(PDO::FETCH_ASSOC)){
  if($row['USUARIO'] == $usuario){
    header("Location:index.php?status=error");
    exit;
  }
}

$sql = "INSERT INTO usuarios VALUES(usu_seq.nextval,
  '$contrasenia', '$nombre', '$apellido', '$direccion', '$zip',
  '$pais', '$estado', '$ciudad', null, '$usuario', '$email')";

$query = $conn->prepare($sql);
$query->execute();

header("Location:index.php?status=ok");

?>
