<?php

include("inc/header.php");
include("inc/conexion.php");

//Actualizar cantidad de prodcuto
foreach ($_POST as $key => $value) {
  $sql = "UPDATE carrito SET cantidad = '$value' WHERE id_usuario = ".$_SESSION['id']." AND id_producto = $key";
  $query = $conn->prepare($sql);
  $query->execute();
}

$sql = "SELECT p.id_producto, nombre_producto, cantidad, precio FROM productos p";
$sql .= " INNER JOIN carrito c";
$sql .= " ON p.id_producto = c.id_producto";
$sql .= " WHERE id_usuario = ".$_SESSION['id'];
$query = $conn->prepare($sql);
$query->execute();
?>

<div class="text-right  ">
    <table class="table">
      <thead>
        <tr>
          <th scope="col"></th>
          <th scope="col">Nombre del producto</th>
          <th scope="col">Precio</th>
          <th scope="col">Cantidad</th>
          <th scope="col">Total</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $pagar = 0;
          while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $id = $row['ID_PRODUCTO'];
            $nombre = $row['NOMBRE_PRODUCTO'];
            $precio = $row['PRECIO'];
            $cantidad = $row['CANTIDAD'];
            $total = $precio * $cantidad;
            $pagar += $total;
        ?>
        <tr class="text-right">
          <form action="carrito.php" method="post">
            <td><button class="badge badge-danger" name="descarrito" value="<?php echo $id; ?>">x</button></td>
          </form>
          <td><?php echo $nombre; ?></td>
          <td><?php echo $precio. " MXN"; ?></td>
          <td><?php echo $cantidad ?></td>
          <td><?php echo $total. " MXN" ?></td>
          <?php } ?>
        </tr>
      </tbody>
    </table>

    <h5>Total: <?php echo $pagar; ?> MXN</h5><br>

    <form class="text-left" action="pagar.php" method="post">
      <div class="row">

        <div class="col-md-5">
          <p>Seleccione el método de pago: </p>
          <select class="form-control" name="pago">
            <?php
              $sql = "SELECT id_tarjeta, num_tarjeta, tipo FROM ref_pago WHERE id_usuario = " . $_SESSION['id'];
              $query = $conn->prepare($sql);
              $query->execute();

              while($row = $query->fetch(PDO::FETCH_ASSOC)){ ?>
              <option value="<?php echo $row['ID_TARJETA'] ?>">
                <?php echo "xxxx-xxxx-xxxx-" . substr($row['NUM_TARJETA'], 12). " " . $row['TIPO']; ?>
              </option>
            <?php } ?>
          </select>
        </div>
        <div class="col-md-3">
          <p>Método de envio: </p>
          <select class="form-control" name="paqueteria">
            <option value="RedPack">RedPack</option>
            <option value="DHL">DHL</option>
            <option value="Estafeta">Estafeta</option>
          </select>
        </div>
        <div class="col-md-4">
          <p>Modalidad de envio: </p>
          <select class="form-control" name="modalidad">
            <?php
              $sql = "SELECT * FROM modalidades";
              $query = $conn->prepare($sql);
              $query->execute();
              while($row = $query->fetch(PDO::FETCH_ASSOC)){ ?>
                <option value="<?php echo $row['ID_MODALIDAD'] ?>"><?php echo $row['DESCRIPCION'] ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
        <?php
          $sql = "SELECT * FROM usuarios WHERE id_usuario = " . $_SESSION['id'];
          $query = $conn->prepare($sql);
          $query->execute();
          $row = $query->fetch(PDO::FETCH_ASSOC);
        ?>
        <div class="row">
          <div class="col-md-8 mb-3 mt-3">
            <label for="direccion">Direccion</label>
            <input type="text" class="form-control" id="direccion" name="direccion"
            value="<?php echo $row['DIRECCION']; ?>" maxlength="250">
            <div class="invalid-feedback">
              <span>Es requerido</span>
            </div>
          </div>
          <div class="col-md-4 mb-3 mt-3">
            <label for="zip">Código postal</label>
            <input type="text" class="form-control" id="zip" name="zip"
            value="<?php echo $row['CODIGO_POSTAL']; ?>" maxlength="5">
            <div class="invalid-feedback">
              <span>Es requerido</span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 mb-3">
            <label for="pais">País</label>
            <input type="text" class="form-control" id="pais" name="pais"
            value="<?php echo $row['PAIS']; ?>" maxlength="20">
            <div class="invalid-feedback">
              <span>Es requerido</span>
            </div>
          </div>
          <div class="col-md-4 mb-3">
            <label for="estado">Estado</label>
            <input type="text" class="form-control" id="estado" name="estado"
            value="<?php echo $row['ESTADO']; ?>" maxlength="20">
            <div class="invalid-feedback">
              <span>Es requerido</span>
            </div>
          </div>
          <div class="col-md-4 mb-3">
            <label for="ciudad">Ciudad</label>
            <input type="text" class="form-control" id="ciudad" name="ciudad"
            value="<?php echo $row['CIUDAD']; ?>" maxlength="20">
            <div class="invalid-feedback">
              <span>Es requerido</span>
            </div>
          </div>
        </div>
        <div class="col-lg-6 float-right pr-0 mt-4">
          <div class="input-group">
            <input type="password" class="form-control" name="confirmar" placeholder="Contraseña" maxlength="12">
            <span class="input-group-btn">
              <button class="btn btn-success" type="submit">Confirmar</button>
            </span>
          </div>
        </div>
    </form>

</div>

<?php

include("inc/jquery.php");
include("inc/footer.php");

?>
