<?php
  session_start();
  if(!isset($_GET['id'])){
    header("Location:index.php?status=error04");
  }
  if(!isset($_SESSION['id'])){
    header("Location:index.php?status=error05");
  }
  include("inc/conexion.php");

  //Añadir carrito
  if(isset($_POST['carrito'])){
    $sql = "INSERT INTO carrito VALUES(".$_SESSION['id'].", " .$_GET['id'].", 1)";
    $query = $conn->prepare($sql);
    $query->execute();
  }
  //Quitar del carrito
  if(isset($_POST['descarrito'])){
    $sql = "DELETE FROM carrito WHERE id_usuario = ".$_SESSION['id']." AND id_producto = ".$_GET['id'];
    $query = $conn->prepare($sql);
    $query->execute();
  }

  //Añadir favorito
  if(isset($_POST['favorito'])){
    $sql = "INSERT INTO favoritos VALUES(".$_SESSION['id'].", ".$_POST['favorito'].")";
    $query = $conn->prepare($sql);
    $query->execute();
  }

  //Quitar favoritos
  if(isset($_POST['desfavorito'])){
    $sql = "DELETE FROM favoritos WHERE id_usuario = ".$_SESSION['id']." AND id_producto = ".$_GET['id'];
    $query = $conn->prepare($sql);
    $query->execute();
  }
  //Comprovar carrito para botón
  $sql = "SELECT * FROM carrito WHERE id_usuario = ".$_SESSION['id']." AND id_producto = " . $_GET['id'];
  $query = $conn->prepare($sql);
  $query->execute();
  $car = false;
  while($row = $query->fetch(PDO::FETCH_ASSOC)){
    $car = true;
  }

  //Comprobar favorito
  $sql = "SELECT * FROM favoritos WHERE id_usuario = ".$_SESSION['id']." AND id_producto = " . $_GET['id'];
  $query = $conn->prepare($sql);
  $query->execute();
  $fav = false;
  while($row = $query->fetch(PDO::FETCH_ASSOC)){
    $fav = true;
  }

  include("inc/header.php");

  $sql = "SELECT p.id_producto, direccion_imagen, nombre_producto, categoria, precio, slider";
  $sql .= " FROM galerias g";
  $sql .= " INNER JOIN productos p";
  $sql .= " ON g.ID_PRODUCTO = p.ID_PRODUCTO";
  $sql .= " WHERE p.id_producto = ". $_GET['id'] ." AND slider = 0 AND ROWNUM = 1";

  $query = $conn->prepare($sql);
  $query->execute();

  $row = $query->fetch(PDO::FETCH_ASSOC);
  $urlImagen = $row['DIRECCION_IMAGEN'];
  $nombre = $row['NOMBRE_PRODUCTO'];
  $precio = $row['PRECIO'];

  if(!isset($nombre)){
    header("Location:index.php");
  }

?>

<div class="row">
  <div class="col-md-6 text-left imagen-producto">
    <img src="img/<?php echo $urlImagen; ?>">
  </div>
  <div class="col-md-6 text-right">
    <h3><?php echo $nombre; ?></h3>
    <h5>Precio: <?php echo $precio ?> MXN</h5>
    <?php if($car){ ?>
      <form action="articulo.php?id=<?php echo $_GET['id']; ?>" method="post">
        <button class="btn btn-outline-secondary mt-5" type="submit" name="descarrito" value="<?php echo $_GET['id'] ?>">
          Quitar del carrito
        </button>
      </form>
    <?php } else {?>
      <form action="articulo.php?id=<?php echo $_GET['id']; ?>" method="post">
        <button class="btn btn-success mt-5" type="submit" name="carrito" value="<?php echo $_GET['id'] ?>">
          Comprar
        </button>
      </form>
    <?php } ?>
    <?php if($fav){ ?>
      <form action="articulo.php?id=<?php echo $_GET['id']; ?>" method="post">
        <button class="btn btn-outline-danger mt-2" type="submit" name="desfavorito" value="<?php echo $_GET['id'] ?>">
          Quitar de favoritos
        </button>
      </form>
    <?php } else { ?>
      <form action="articulo.php?id=<?php echo $_GET['id']; ?>" method="post">
        <button class="btn btn-outline-warning mt-2" type="submit" name="favorito" value="<?php echo $_GET['id'] ?>">
          Añadir a favoritos
        </button>
      </form>
    <?php } ?>
  </div>

</div>

<?php
  include("inc/jquery.php");
  include("inc/footer.php");
?>
