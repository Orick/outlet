<?php

  require_once("conexion.php");

  $sql = "SELECT usuario FROM usuarios";
  $query = $conn->prepare($sql);
  $query->execute();

  echo '{';
    echo '"users":[';
      while($row = $query->fetch(PDO::FETCH_ASSOC)){
        echo '"' . $row['USUARIO'] . '",';
      }
      echo '""';
    echo ']';
  echo '}';

?>
