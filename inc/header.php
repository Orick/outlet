<!doctype html>
<html lang="es">
  <head>
    <title>Outlet</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="css/master.css">
  </head>
  <body>
    <?php

    if(!isset($_SESSION['usuario'])){
      session_start();
    }

    include("inc/alertas.php");

    ?>


    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand" href="."><h3>Yezenia</h3></a>
      <?php if(!isset($_SESSION['usuario'])){ ?>
        <div class="ml-auto mr-4">
          <a class="text-dark" href="#" data-toggle="modal" data-target="#login">Iniciar Sesión</a>
          |
          <a class="text-dark" href="#" data-toggle="modal" data-target="#registro">Registrarse</a>
        </div>
      <?php } else {?>
        <?php //Revisar y desplegar la opción de ver carrito
        include("inc/conexion.php");
          $sql = "SELECT COUNT(*) AS articulos FROM carrito WHERE id_usuario = " . $_SESSION['id'];
          $query = $conn->prepare($sql);
          $query->execute();
          $linkCarr = false;
          $row = $query->fetch(PDO::FETCH_ASSOC);
          if(isset($row['ARTICULOS']) && $row['ARTICULOS'] > 0){
            $linkCarr = true;
            $html = "<a class='mr-4 text-dark' href='carrito.php'>Carrito <span class='badge badge-warning'>".$row['ARTICULOS']."</span></a>";
          }
        ?>
        <div class="ml-auto mr-4">
          <?php if(isset($html)){ echo $html;} ?>
          <span class="text-dark">Bienvenido/a <a href="perfil.php"><?php echo $_SESSION['usuario']; ?></a></span>
          |
          <a class="text-dark" href="logout.php">Cerrar sesión</a>
        </div>
      <?php } ?>
      <form class="form-inline" action="buscar.php" method="get">
        <input class="form-control mr-sm-2" type="search" placeholder="Buscar" name="buscar" aria-label="Buscar">
        <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Buscar</button>
      </form>
    </nav>
    <div class="container">
    <div class=" mt-5 row">
      <aside class="col-md-2 col-lg-3 mh-100 navegacion">
        <a class="text-dark" href="#"><h4>Catálogo</h4></a>
        <hr>
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link text-dark" href="buscar.php?categoria=Vestido">Vestidos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" href="buscar.php?categoria=Blusa">Blusas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" href="buscar.php?categoria=Falda">Faldas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" href="buscar.php?categoria=Pantalon">Pantalones</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" href="buscar.php?categoria=Abrigo">Abrigos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" href="buscar.php?categoria=otros">Otros</a>
          </li>
        </ul>
      </aside>

      <?php if(!isset($_SESSION['usuario'])){ ?>
      <!-- Modal REGISTRO-->
      <div class="modal fade" id="registro" tabindex="-1" role="dialog" aria-labelledby="registroModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title lang" id="registroLabel">Registrarse</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
              <div class="modal-body">
                <form id="formularioRegistro" action="registrar.php" method="post" autocomplete="off">
                  <div class="row">
                    <div class="col-md-6 mb-3">
                      <label for="usuairo">Usuario</label>
                      <input type="text" class="form-control" id="usuario" name="usuario" maxlength="10">
                      <div class="invalid-feedback" id="mensaje-primario">
                        <span>El usuario debe de tener entre 4 y 10 caracteres</span>
                      </div>
                      <div class="invalid-feedback" id="taked-user">
                        <span>Nombre de usuario no disponible</span>
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="email">E-mail</label>
                      <input type="email" class="form-control" id="email" name="email" maxlength="150">
                      <div class="invalid-feedback">
                        <span>Error e-mail no valido</span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 mb-3">
                      <label for="nombre">Nombre(s)</label>
                      <input type="text" class="form-control" id="nombre" name="nombre" maxlength="100">
                      <div class="invalid-feedback">
                        <span>Es requerido</span>
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="apellidos">Apellidos</label>
                      <input type="text" class="form-control" id="apellidos" name="apellidos" maxlength="100">
                      <div class="invalid-feedback">
                        <span>Es requerido</span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8 mb-3">
                      <label for="direccion">Direccion</label>
                      <input type="text" class="form-control" id="direccion" name="direccion" maxlength="250">
                      <div class="invalid-feedback">
                        <span>Es requerido</span>
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="zip">Código postal</label>
                      <input type="text" class="form-control" id="zip" name="zip" maxlength="5">
                      <div class="invalid-feedback">
                        <span>Es requerido</span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 mb-3">
                      <label for="pais">País</label>
                      <input type="text" class="form-control" id="pais" name="pais" maxlength="20">
                      <div class="invalid-feedback">
                        <span>Es requerido</span>
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="estado">Estado</label>
                      <input type="text" class="form-control" id="estado" name="estado" maxlength="20">
                      <div class="invalid-feedback">
                        <span>Es requerido</span>
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="ciudad">Ciudad</label>
                      <input type="text" class="form-control" id="ciudad" name="ciudad" maxlength="20">
                      <div class="invalid-feedback">
                        <span>Es requerido</span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 mb-3">
                      <label for="contrasena">Contraseña</label>
                      <input type="password" class="form-control" id="contrasena" name="contrasenia" maxlength="12">
                      <div class="invalid-feedback">
                        <span>La contraseña debe tener entre 4 y 12 caracteres</span>
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="confirmar">Confirmar contraseña</label>
                      <input type="password" class="form-control" id="confirmar" maxlength="12">
                      <div class="invalid-feedback">
                        <span>Las contraseñas no coinciden</span>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Registrarse</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal LOGIN-->
        <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <form id="formularioSesion" action="login.php" method="post" autocomplete="on">
                <div class="modal-header">
                  <h5 class="modal-title lang" id="incicioLaebl">Iniciar sesión</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-6 mb-3">
                      <label for="usuairo">Usuario</label>
                      <input type="text" class="form-control" id="usuario-log" name="usuario" required maxlength="10">
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="contrasena">Contraseña</label>
                      <input type="password" class="form-control" id="contrasena-log" name="contrasenia" required maxlength="12">
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-primary" >Iniciar sesión</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="col-md-10 col-lg-9 ml-auto text-right">
