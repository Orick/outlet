<?php

if(isset($_GET['status'])){
  if($_GET['status'] == 'ok'){
    echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
          ¡Usuario registrado!
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>

          </button>
        </div>";
  }
  if($_GET['status'] == 'error'){
    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
          Hubo un error con el registro.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
  }
  if($_GET['status'] == 'error_inicio'){
    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
          Usuario/Contraseña no validos.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
  }
  if($_GET['status'] == 'logout'){
    echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
          Sesión cerrada correctamente.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>

          </button>
        </div>";
  }
  if($_GET['status'] == 'error02'){
    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
          Error al actualizar la información.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
  }
  if($_GET['status'] == 'error03'){
    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
          Contraseña incorrecta.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>

          </button>
        </div>";
  }
  if($_GET['status'] == 'error04'){
    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
          Datos de tarjeta de credito incorrectos.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>

          </button>
        </div>";
  }
  if($_GET['status'] == 'error04'){
    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
          Articulo no encontrado.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>

          </button>
        </div>";
  }
  if($_GET['status'] == 'error05'){
    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
          Debes registarte para llevar a cabo esta acción.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>

          </button>
        </div>";
  }
  if($_GET['status'] == 'actualizado'){
    echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
          La información a sido actualizada correctamente.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>

          </button>
        </div>";
  }
  if($_GET['status'] == 'tar_ok'){
    echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
          Método de pago añadido con exito.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>

          </button>
        </div>";
  }
  if($_GET['status'] == 'compra_ok'){
    echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
          Operación realizada con exito, ¡Gracias por su compra!.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>

          </button>
        </div>";
  }
}

?>
