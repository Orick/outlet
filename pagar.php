<?php

session_start();

include("inc/conexion.php");

if( $_POST['direccion'] == "" || $_POST['ciudad'] == "" || $_POST['estado'] == ""
  || $_POST['pais'] == "" || $_POST['zip'] == "" || !isset($_POST['confirmar'])){
    header("Location:comprar.php?status=error02");
    die;
}

if(!is_numeric($_POST['zip'])){
  header("Location:comprar.php?status=error02");
  exit;
} else if($_POST['zip'] > 99999){
  header("Location:comprar.php?status=error02");
  exit;
}

$consulta = "SELECT contrasena FROM usuarios WHERE id_usuario = " . $_SESSION['id'];
$query = $conn->prepare($consulta);
$query->execute();

while($row = $query->fetch(PDO::FETCH_ASSOC)){
  if($_POST['confirmar'] !== $row['CONTRASENA']){
    header("Location:comprar.php?status=error03");
    die;
  }
}

$destino = $_POST['direccion'] ." ". $_POST['ciudad'] ." ". $_POST['estado'] ." ". $_POST['pais'] . " " . $_POST['zip'];

$sql = "INSERT INTO pedidos VALUES(ped_seq.NEXTVAL, '$destino', '".$_POST['paqueteria'];
$sql .= "', 'Pendiente', ".$_SESSION['id'].", ".$_POST['modalidad'].", ".$_POST['pago'].")";
$query = $conn->prepare($sql);
$query->execute();

$sql = "SELECT id_pedido FROM pedidos WHERE id_usuario = " . $_SESSION['id'] . "AND ROWNUM = 1 ORDER BY id_pedido DESC";
$query = $conn->prepare($sql);
$query->execute();
$pedidoId = $query->fetch(PDO::FETCH_ASSOC);
$pedidoId = $pedidoId['ID_PEDIDO'];

$sql = "SELECT * FROM carrito WHERE id_usuario = " . $_SESSION['id'];
$query = $conn->prepare($sql);
$query->execute();
while($row = $query->fetch(PDO::FETCH_ASSOC)){
  $sql = "INSERT INTO productos_pedidos VALUES(".$row['ID_PRODUCTO'].", $pedidoId, ".$row['CANTIDAD'].")";
  $insertar = $conn->prepare($sql);
  $insertar->execute();
}

$sql = "DELETE FROM carrito WHERE id_usuario = " . $_SESSION['id'];
$query = $conn->prepare($sql);
$query->execute();

header("Location:perfil.php?status=compra_ok");

?>
