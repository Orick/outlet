<?php

  session_start();
  if(!isset($_SESSION['usuario'])){
    header("Location:index.php");
    exit;
  }
  include("inc/header.php");
  include("inc/conexion.php");

  $sql = "SELECT * FROM usuarios WHERE ROWNUM = 1 AND id_usuario = " . $_SESSION['id'];

  $query = $conn->prepare($sql);
  $query->execute();

  while($row = $query->fetch(PDO::FETCH_ASSOC)){
    $email = $row['EMAIL'];
    $nombre = $row['NOMBRE'];
    $apellido = $row['APELLIDO'];
    $direccion = $row['DIRECCION'];
    $zip = $row['CODIGO_POSTAL'];
    $pais = $row['PAIS'];
    $estado = $row['ESTADO'];
    $ciudad = $row['CIUDAD'];
  }

  $sql = "SELECT * FROM ref_pago WHERE id_usuario = " . $_SESSION['id'];
  $query = $conn->prepare($sql);
  $query->execute();

  while($pagos[] = $query->fetch(PDO::FETCH_ASSOC)){
    /*$numeroTar[] = $pagos['NUM_TARJETA'];
    $fecha[] = $pagos['FECHA_VENCIMIENTO'];
    $seguridad[] = $pagos['SEGURIDAD'];
    $tipo[] = $pagos['TIPO'];*/
  }

  array_pop($pagos);

  function SeleccionEstado($estado){
    if($estado == "Pendiente"){
      return "table-info";
    }
    if($estado == "Cancelado"){
      return "table-danger";
    }
    if($estado == "En camino"){
      return "table-warning";
    }
    if($estado == "Recibido"){
      return "table-success";
    }

  }

?>

  <ul class="nav nav-tabs" id="tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="perfil-tab" data-toggle="tab" href="#perfil" role="tab" aria-controls="home" aria-selected="true">Perfil</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="favoritos-tab" data-toggle="tab" href="#favoritos" role="tab" aria-controls="home" aria-selected="false">Favoritos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pedidos-tab" data-toggle="tab" href="#pedidos" role="tab" aria-controls="profile" aria-selected="false">Pedidos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contacto-tab" data-toggle="tab" href="#contacto" role="tab" aria-controls="contact" aria-selected="false">Contacto</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pagos-tab" data-toggle="tab" href="#pagos" role="tab" aria-controls="contact" aria-selected="false">Métodos de pago</a>
  </li>
</ul>
<div class="tab-content text-left" id="servicio-tad">

  <div class="tab-pane fade show active" id="perfil" role="tabpanel" aria-labelledby="perfil-tab">
    <form id="formularioActualizar" action="editar.php" method="post" autocomplete="off" class="mt-3">
      <div class="row">
        <div class="col-md-6 mb-3">
          <label for="usuairo">Usuario</label>
          <input type="text" class="form-control" id="usuario" name="usuario" disabled
          value="<?php echo $_SESSION['usuario']; ?>">
        </div>
        <div class="col-md-6 mb-3">
          <label for="email">E-mail</label>
          <input type="email" class="form-control" id="email" name="email"
          value="<?php echo $email; ?>" disabled>
          <div class="invalid-feedback">
            <span>Error e-mail no valido</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 mb-3">
          <label for="nombre">Nombre(s)</label>
          <input type="text" class="form-control" id="nombre" name="nombre"
          value="<?php echo $nombre; ?>" maxlength="100">
          <div class="invalid-feedback">
            <span>Es requerido</span>
          </div>
        </div>
        <div class="col-md-6 mb-3">
          <label for="apellidos">Apellidos</label>
          <input type="text" class="form-control" id="apellidos" name="apellidos"
          value="<?php echo $apellido; ?>" maxlength="100">
          <div class="invalid-feedback">
            <span>Es requerido</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 mb-3">
          <label for="direccion">Direccion</label>
          <input type="text" class="form-control" id="direccion" name="direccion"
          value="<?php echo $direccion; ?>" maxlength="250">
          <div class="invalid-feedback">
            <span>Es requerido</span>
          </div>
        </div>
        <div class="col-md-4 mb-3">
          <label for="zip">Código postal</label>
          <input type="text" class="form-control" id="zip" name="zip"
          value="<?php echo $zip; ?>" maxlength="5">
          <div class="invalid-feedback">
            <span>Es requerido</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 mb-3">
          <label for="pais">País</label>
          <input type="text" class="form-control" id="pais" name="pais"
          value="<?php echo $pais; ?>" maxlength="20">
          <div class="invalid-feedback">
            <span>Es requerido</span>
          </div>
        </div>
        <div class="col-md-4 mb-3">
          <label for="estado">Estado</label>
          <input type="text" class="form-control" id="estado" name="estado"
          value="<?php echo $estado; ?>" maxlength="20">
          <div class="invalid-feedback">
            <span>Es requerido</span>
          </div>
        </div>
        <div class="col-md-4 mb-3">
          <label for="ciudad">Ciudad</label>
          <input type="text" class="form-control" id="ciudad" name="ciudad"
          value="<?php echo $ciudad; ?>" maxlength="20">
          <div class="invalid-feedback">
            <span>Es requerido</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 mb-3">
          <label for="contrasena">Contraseña</label>
          <input type="password" class="form-control" id="contrasena" name="contrasenia" maxlength="12">
          <div class="invalid-feedback">
            <span>La contraseña debe tener entre 4 y 12 caracteres</span>
          </div>
        </div>
        <div class="col-md-6 mb-3">
          <label for="confirmar">Confirmar contraseña</label>
          <input type="password" class="form-control" id="confirmar" name="confirmar" maxlength="12">
          <div class="invalid-feedback">
            <span>Las contraseñas no coinciden</span>
          </div>
        </div>
      </div>
      <div class="modal-footer pr-0">
        <button type="reset" class="btn btn-secondary">Reiniciar</button>
        <div class="col-lg-6">
          <div class="input-group">
            <input type="password" class="form-control" name="confirmar" placeholder="Contraseña" maxlength="12">
            <span class="input-group-btn">
              <button class="btn btn-primary" type="submit">Actualizar</button>
            </span>
          </div>
        </div>
      </div>
    </form>
  </div>

  <div class="tab-pane fade" id="favoritos" role="tabpanel" aria-labelledby="favoritos-tab">

    <div class="row justify-content-between mt-3">
    <?php

    //Empieza contenido de la página
    $sql = "SELECT p.id_producto, nombre_producto, precio, direccion_imagen";
    $sql .= " FROM favoritos f";
    $sql .= " INNER JOIN productos p";
    $sql .= " ON f.id_producto = p.id_producto";
    $sql .= " INNER JOIN galerias g";
    $sql .= " ON p.id_producto = g.id_producto";
    $sql .= " WHERE f.id_usuario = ".$_SESSION['id']." AND slider = 0";

    $query = $conn->prepare($sql);
    $query->execute();
    $flag = true;

    while($row = $query->fetch(PDO::FETCH_ASSOC)){ ?>
      <div class="card border-dark col-md-4">
        <img class="card-img-top" src="<?php echo "img/".$row['DIRECCION_IMAGEN']; ?>">
        <div class="card-body">
          <h4 class="card-title"><?php echo $row['NOMBRE_PRODUCTO']; ?></h4>
          <div class="mt-4">
            <p><?php echo $row['PRECIO']."MXN"; ?>
              <a href="articulo.php?id=<?php echo $row['ID_PRODUCTO']; ?>">
                <button type="button" class="btn-sm btn-success float-right">Comprar</button>
              </a>
            </p>
          </div>
        </div>
      </div>

    <?php
      $flag = false;
        }
      if($flag){
        echo "Lista de favoritos vacía.";
      }
    ?>
    </div>
  </div>

  <div class="tab-pane fade" id="pedidos" role="tabpanel" aria-labelledby="pedidos-tab">

    <table class="table table-hover">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Destino</th>
          <th scope="col">Paqueteria</th>
          <th scope="col">Estado</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $sql = "SELECT * FROM pedidos WHERE id_usuario = ". $_SESSION['id'];
        $query = $conn->prepare($sql);
        $query->execute();
        while($row = $query->fetch(PDO::FETCH_ASSOC)){ ?>
        <tr class="<?php echo SeleccionEstado($row['DETALLES']); ?>">
          <td><strong><?php echo $row['ID_PEDIDO']; ?></strong></td>
          <td><?php echo $row['DESTINO']; ?></td>
          <td><?php echo $row['PAQUETERIA']; ?></td>
          <td><?php echo $row['DETALLES'] ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>

  </div>
  <div class="tab-pane fade" id="contacto" role="tabpanel" aria-labelledby="contact-tab">

    <div class="mt-3">
      <form>
        <div class="form-group">
          <label for="exampleFormControlInput1">E-mail: </label>
          <input type="email" class="form-control" id="email" name="email"
          value="<?php echo $email; ?>" disabled>
        </div>
        <div class="form-group">
          <label for="mensaje">Mensaje: </label>
          <textarea class="form-control" id="mensaje" name="mensaje" rows="4"></textarea>
        </div>

        <button type="button" class="btn btn-primary float-right">Enviar</button>
      </form>
    </div>

  </div>

  <div class="tab-pane fade" id="pagos" role="tabpanel" aria-labelledby="contact-tab">

    <div class="mt-3">

      <?php
        if(isset($pagos[0])){
        for ($i=0; $i < count($pagos); $i++) {
      ?>
      <form class="container col-md-6" id="borrarTarjeta" action="borrar_tarjeta.php" method="post">
        <!--Previene auto completado en google chrome-->
        <input style="display:none">
        <input type="password" style="display:none">
        <!-- ********************************* -->
        <div class="row">
          <div class="col-md-12 mb-3">
            <p><?php echo "xxxx-xxxx-xxxx-" . substr($pagos[$i]['NUM_TARJETA'], 12); ?></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <p>xxx</p>
          </div>
          <div class="col-md-3 mb-3">
                <p><?php echo "Mes: " . $pagos[$i]['MES']; ?></p>
          </div>
          <div class="col-md-3 mb-3">
                <p><?php echo "Año: " . $pagos[$i]['ANIO']; ?></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?php
              if($pagos[$i]['TIPO'] == 'Master Card'){
                $ico = "master-card.svg";
              } else if($pagos[$i]['TIPO'] == 'Visa'){
                $ico = "visa.png";
              }
            ?>
            <img class="icon-card" src="assets/<?php echo $ico; ?>">
          </div>
          <div class="col-md-6 text-right mt-1">
            <button type="submit" class="btn btn-danger" name="id" value="<?php echo $pagos[$i]['ID_TARJETA']; ?>">
              Borrar
            </button>
          </div>
        </div>
      </form>
      <hr>
      <?php }} ?>

      <form class="container col-md-6" id="anadirTarjeta" method="post" action="nueva_tarjeta.php" autocomplete="off">
        <!--Previene auto completado en google chrome-->
        <input style="display:none">
        <input type="password" style="display:none">
        <!-- ********************************* -->
        <div class="row">
          <div class="col-md-12 mb-3">
            <label for="tarjeta">No. de tarjeta:</label>
            <input type="text" class="form-control" id="tarjeta" name="tarjeta" maxlength="16" required>
            <div class="invalid-feedback">
              Tarjeta no valida.
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="exp">EXP:</label>
            <input type="text" class="form-control" id="exp" name="exp" maxlength="3" required>
            <div class="invalid-feedback">
              EXP no valido.
            </div>
          </div>
          <div class="col-md-3 mb-3">
            <div class="form-group">
              <label for="mes">Mes:</label>
              <select class="form-control" id="mes" name="mes">
                <option value="1">01</option>
                <option value="2">02</option>
                <option value="3">03</option>
                <option value="4">04</option>
                <option value="5">05</option>
                <option value="6">06</option>
                <option value="7">07</option>
                <option value="8">08</option>
                <option value="9">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
            </div>
          </div>
          <div class="col-md-3 mb-3">
            <div class="form-group">
              <label for="anio">Año:</label>
              <select class="form-control" id="anio" name="anio">
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="tipo">Compañia:</label>
              <select class="form-control" id="tipo" name="tipo">
                <option value="Master Card">Master Card</option>
                <option value="Visa">Visa</option>
              </select>
            </div>
          </div>
        </div>
          <div class="row">
            <div class="col-md-12">
              <div class="fomr-group float-right">
                <button class="btn btn-primary" type="submit">Añadir</button>
              </div>
            </div>
          </div>
        </div>
      </form>


    </div>

  </div>


</div>
</div>
<!-- Optional JavaScript -->
<?php
include("inc/jquery.php");
include("inc/footer.php");
?>
