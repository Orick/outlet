$(document).ready(function(){
  //Carrusel
  $('.carousel').carousel();

  //Validación usuario con AJAX
  let url='inc/userauth.php';
  let flagUser = false;
  $.getJSON(url, function(usuarios){
    $("#usuario").keyup(function(){
      flagUser = false;
      if($("#usuario").val().length > 3){
        for(let i = 0; i < usuarios.users.length; i++){
          if(usuarios.users[i] == $("#usuario").val()){
            //Si el nombre de usuario no esta disponible
            $("#formularioRegistro button[type=submit]").prop("disabled", true);
            $("#taked-user").show();
            $("#usuario").removeClass("is-valid");
            $("#usuario").addClass("is-invalid");
            $("#mensaje-primario").hide();
            flagUser = true
          } else if(!flagUser) {
            //Si el nombre de usuario esta disponible
            $("#formularioRegistro button[type=submit]").prop("disabled", false);
            $("#taked-user").hide();
            $("#usuario").addClass("is-valid");
            $("#usuario").removeClass("is-invalid");
          }
        }
      }
    });
  });//Termina AJAX, validación de nombre de usuairo

  //Vaidación contraseña
  var $password = $("#contrasena");
  var $confirmPassword = $("#confirmar");
  let band = $("#formularioActualizar").length;

  function isPasswordValid(){
    if($password.val().length > 0){
      return ($password.val().length > 3 && $password.val().length < 13);
    } else{
      return ($password.val().length > 3 && $password.val().length < 13) || band ;
    }

  }

  function arePasswordsMatching(){
    if($password.val().length > 0){
      return ($password.val() === $("#confirmar").val() && $password.val() != "");
    } else{
      return ($password.val() === $("#confirmar").val() && $password.val() != "") || band;
    }
  }

  function canSubmit(){
    return !(isPasswordValid() && arePasswordsMatching());
  }

  function passwordEvent(){
    if(isPasswordValid()){
      //$password.next().hide();
      $password.removeClass("is-invalid");
      $password.addClass("is-valid");
    } else{
      $password.removeClass("is-valid");
      $password.addClass("is-invalid");
    }
  }

  function confirmPasswordEvent(){
    if(arePasswordsMatching()){
      $confirmPassword.removeClass("is-invalid");
      $confirmPassword.addClass("is-valid");
    } else{
      $confirmPassword.removeClass("is-valid");
      $confirmPassword.addClass("is-invalid");
    }
  }

  function enableSubmitEvent(){
    $("#formularioRegistro button[type=submit], #formularioActualizar button[type=submit]").prop("disabled", canSubmit());
  }

  $password.focus(passwordEvent).keyup(passwordEvent).keyup(confirmPasswordEvent).keyup(enableSubmitEvent);
  $confirmPassword.focus(confirmPasswordEvent).keyup(confirmPasswordEvent).keyup(enableSubmitEvent);

  enableSubmitEvent();


  //Validación formulario de registro
  $('#formularioRegistro').submit(function(event){
    //Quita la clase de campo invalido para volvere a comprobar
    $('#formularioRegistro input, #formularioActualizar input').removeClass("is-invalid");

    //Conseguir los datos del formulario sesion
    let usuario = $('#formularioRegistro #usuario').val();
    let email = $('#formularioRegistro #email').val();

    let campos = [
      $('#usuario'),
      $('#email'),
      $('#nombre'),
      $('#apellidos'),
      $('#direccion'),
      $('#zip'),
      $('#pais'),
      $('#estado'),
      $('#ciudad'),
      $('#contrasena')
    ];
    //La contraseña se deja para más tarde para darle un "trato especial"

    if(usuario.length < 4 || usuario.length > 10){
      $('#usuario').addClass("is-invalid");
      $('#taked-user').hide();
      $('#mensaje-primario').show();
      event.preventDefault();
    }
    if(email.length < 5){
      $('#email').addClass("is-invalid");
      event.preventDefault()
    }
    for(let i = 0; i < campos.length; i++){
      if(campos[i].val().length < 3){
        campos[i].addClass("is-invalid");
        event.preventDefault();
      } else {
        campos[i].addClass("is-valid");
      }
    }
  }); // Términa validación formulario de registro

  //Validación inicio de sesión
  $('#formularioSesion').submit(function(event){
    //Quita la clase de campo invalido para volvere a comprobar
    $('#formularioSesion input').removeClass("is-invalid");

    let usuario = $('#usuario-log').val();
    let contrasena = $('#contrasena-log').val();

    if(usuario.length < 4 || usuario.length > 10){
      $('#usuario-log').addClass("is-invalid");
      event.preventDefault();
    }

    if(contrasena.length < 4 || contrasena.length > 12){
      $('#contrasena-log').addClass("is-invalid");
      event.preventDefault();
    }

    $("#usuario-log").keyup(function(){
      $(this).removeClass("is-invalid");
    });
    $("#contrasena-log").keyup(function(){
      $(this).removeClass("is-invalid");
    });

  }); //Términa validacón inicio de sesión

  //Validación actualizar Perfil
  $('#formularioActualizar').submit(function(event){
    $('#formularioActualizar input').removeClass("is-invalid");
    let campos = [
      $('#usuario'),
      $('#email'),
      $('#nombre'),
      $('#apellidos'),
      $('#direccion'),
      $('#zip'),
      $('#pais'),
      $('#estado'),
      $('#ciudad')
    ];

    for(let i = 0; i < campos.length; i++){
      if(campos[i].val().length < 3){
        campos[i].addClass("is-invalid");
        event.preventDefault();
      } else {
        campos[i].addClass("is-valid");
      }
    }
  }); //Termina validación de cambio de información de perfil

  //Confirmación borrar método de pago
  $('#borrarTarjeta').submit(function(event){
    if(!confirm("¿Esta seguro de que quiere borrar esta tarjeta?")){
      event.preventDefault();
    }
  });//Termina confirmación borrar metodo de pago

  //Validación añadir método de pagos
  $('#anadirTarjeta').submit(function(event){
    $('#anadirTarjeta input, #anadirTarjeta select').removeClass("is-invalid");

    if($('#tarjeta').val().length != 16){
      $('#tarjeta').addClass("is-invalid");
      event.preventDefault();
    }

    if($('#exp').val().length != 3){
      $('#exp').addClass("is-invalid");
      console.log($('#exp').val().lenght);
      event.preventDefault();
    }

    if($('#mes').val() < 1 || $('#mes').val() > 12){
      $('#mes').addClass("is-invalid");
      event.preventDefault();
    }

    if($('#anio').val() < 17 || $('#anio').val() > 28){
      $('#anio').addClass("is-invalid");
      console.log($('#exp').val().lenght);
      event.preventDefault();
    }

    if($('#tipo').val() == "Master Card" || $('#tipo').val() == "Visa"){
      //OK
    } else {
      $('#tipo').addClass("is-invalid");
      event.preventDefault();
    }
  }); // Termina validación de añadir tarjeta

});
