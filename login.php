<?php

  session_start();
  require_once("inc/conexion.php");

  $username = trim(filter_input(INPUT_POST, 'usuario', FILTER_SANITIZE_STRING));
  $contrasena = trim(filter_input(INPUT_POST, 'contrasenia', FILTER_SANITIZE_STRING));

  $sql = "SELECT * FROM usuarios WHERE usuario = '$username' AND ROWNUM = 1";

  $query = $conn->prepare($sql);
  $query->execute();

  while($row = $query->fetch(PDO::FETCH_ASSOC)){
    if($row['USUARIO'] === $username && $row['CONTRASENA'] === $contrasena){
      $_SESSION['usuario'] = $username;
      $_SESSION['id'] = $row['ID_USUARIO'];
      header("Location:index.php");
      exit;
    }else{
      header("Location:index.php?status=error_inicio");
      exit;
    }
  }
  header("Location:index.php?status=error_inicio");
  exit;

?>
