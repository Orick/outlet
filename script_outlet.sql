--Usuarios
CREATE SEQUENCE usu_seq START WITH 1;
--Tarjetas
CREATE SEQUENCE tar_seq START WITH 1;
--Carritos
CREATE SEQUENCE car_seq START WITH 1;
--Productos
CREATE SEQUENCE pro_seq START WITH 1;
--Pedidos
CREATE SEQUENCE ped_seq START WITH 1;
--Bodegas
CREATE SEQUENCE bod_seq START WITH 1;
--Empleados
CREATE SEQUENCE emp_seq START WITH 1;
--Puestos
CREATE SEQUENCE pue_seq START WITH 1;
--Modalidad
CREATE SEQUENCE mod_seq START WITH 1;
--Galerias
CREATE SEQUENCE gal_seq START WITH 1;

@ c:/outlet.sql;

--A�adir una columna faltante a la tabla galerias
ALTER TABLE galerias ADD slider NUMBER(1,0);

--Script ejemplo registro de un usuario
INSERT INTO usuarios VALUES(1,
  '1234', 'Senior Oracle', 'sql plus', 'Arbol de Pinos', '87605',
  'Estados Unidos', 'Los Angeles', 'Palo Alto', null, 'oracle', 'oracle@hotmail.com');

--Script ejemplo nuevo producto
INSERT INTO productos VALUES(1, 'Vestido Negro y Flores', 'Vestido', 
  'negro', 1799, 1200);
INSERT INTO productos VALUES(2, 'Vestido rojo dos piezas', 'Vestido', 
  'rojo', 1599, 999);
INSERT INTO productos VALUES(3, 'Vestido azul marino-plateado', 'Vestido', 
  'azul', 2150, 1570);
INSERT INTO productos VALUES(4, 'Vestido azul dos piezas', 'Vestido', 
  'negro', 1499, 899);
INSERT INTO productos VALUES(5, 'Tema de flores', 'Vestido', 
  'negro', 899, 599);
INSERT INTO productos VALUES(6, 'Levis 501', 'Pantalon', 
  'azul', 899, 599);
  
--Uso de galerias
INSERT INTO galerias VALUES(gal_seq.NEXTVAL, 'vestido001.jpeg', 1, 1);
INSERT INTO galerias VALUES(gal_seq.NEXTVAL, 'vestido002.jpeg', 2, 1);
INSERT INTO galerias VALUES(gal_seq.NEXTVAL, 'vestido003.jpeg', 3, 1);
INSERT INTO galerias VALUES(gal_seq.NEXTVAL, 'vestido004.jpeg', 4, 1);
INSERT INTO galerias VALUES(gal_seq.NEXTVAL, 'vestido005.jpeg', 5, 0);

INSERT INTO galerias VALUES(gal_seq.NEXTVAL, 'vestido006.jpg', 1, 0);
INSERT INTO galerias VALUES(gal_seq.NEXTVAL, 'vestido007.jpg', 2, 0);
INSERT INTO galerias VALUES(gal_seq.NEXTVAL, 'vestido008.jpg', 3, 0);
INSERT INTO galerias VALUES(gal_seq.NEXTVAL, 'vestido009.jpg', 4, 0);

INSERT INTO galerias VALUES(gal_seq.NEXTVAL, 'pantalon01.jpeg', 6, 0);

--Ver galeria de un producto por nombre
SELECT direccion_imagen FROM galerias g 
  INNER JOIN productos p 
  ON g.ID_PRODUCTO = p.ID_PRODUCTO
  WHERE p.NOMBRE_PRODUCTO = 'Vestido Negro y Flores';
  
--Ver galeria de un producto por id
SELECT p.id_producto, direccion_imagen, nombre_producto, categoria, precio, slider FROM galerias g 
  INNER JOIN productos p 
  ON g.ID_PRODUCTO = p.ID_PRODUCTO
  WHERE p.id_producto = 4 AND slider = 0 AND ROWNUM = 1;
  
--Mostrar imagenes para el slider
SELECT direccion_imagen, slider, nombre_producto, precio, p.id_producto FROM galerias g
INNER JOIN productos p
ON g.id_producto = p.id_producto
WHERE ROWNUM <= 5 AND SLIDER = 1 ORDER BY id_imagen DESC;

--Primer intento mostar imagenes para la p�gina principal
SELECT direccion_imagen, slider, nombre_producto, precio, p.id_producto FROM galerias g
INNER JOIN productos p
ON g.id_producto = p.id_producto
WHERE ROWNUM <= 8 AND SLIDER = 0 ORDER BY id_imagen DESC;

--Mostrar mostar p�gina principal sin repetir ningun producto
SELECT direccion_imagen, slider, nombre_producto, precio, p.id_producto 
FROM galerias g
INNER JOIN productos p
ON g.id_producto = p.id_producto
WHERE ROWNUM <= 8 AND SLIDER = 0 AND p.id_producto = ANY(
  SELECT DISTINCT p.id_producto from productos p
  INNER JOIN galerias g
  ON p.id_producto = g.id_producto
)
ORDER BY id_imagen DESC;
  
--A�adir nueva tarjeta
INSERT INTO REF_PAGO VALUES(tar_seq.NEXTVAL, '1212343456567878', 789, 'Visa', 1, 2, 19);
INSERT INTO REF_PAGO VALUES(tar_seq.NEXTVAL, '4242424242424242', 123, 'Master Card', 1, 12, 22);
INSERT INTO ref_pago VALUES(tar_seq.NEXTVAL, '1234567890123456', 213, 'Master Card', 1, 07, 24);


--Ver tarjetas de un usuario especifico
SELECT * FROM ref_pago WHERE id_usuario = 1;

--Corregir tabla de tarjetas
ALTER TABLE ref_pago add(mes INTEGER, anio INTEGER);

--Busqueda por nombre
SELECT p.id_producto, nombre_producto, precio, direccion_imagen, slider FROM productos p
INNER JOIN galerias g
ON p.id_producto = g.id_producto
WHERE UPPER(nombre_producto) LIKE UPPER('%Vestido azul%');

--A�adir articulo a favoritos
INSERT INTO favoritos values(1, 4);

--Obtener favoritos junto con imagenes
SELECT * FROM favoritos f
INNER JOIN productos p
ON f.id_producto = p.id_producto
INNER JOIN galerias g
ON p.id_producto = g.id_producto;

--A�adir articulo al carrito
INSERT INTO carrito VALUES(1,1,1);

--Revisar numero de articulos en el carrito
SELECT COUNT(*) as articulos FROM carrito WHERE id_usuario = 1;

--Mostrar lista de productos en el carrito
SELECT p.id_producto, nombre_producto, cantidad, precio FROM productos p
INNER JOIN carrito c 
ON p.id_producto = c.id_producto
WHERE id_usuario = 1;

--A�adir forein key a pedidos
ALTER TABLE pedidos ADD id_tarjeta INTEGER NOT NULL;

ALTER TABLE pedidos ADD CONSTRAINT FK_ID_TARGETA_REF_PAGO
  FOREIGN KEY (id_tarjeta) REFERENCES ref_pago(id_tarjeta);

--A�adir modalidades de envio
INSERT INTO modalidades VALUES(mod_seq.NEXTVAL, 'Envio estandar');
INSERT INTO modalidades VALUES(mod_seq.NEXTVAL, 'Envio express');

--Hacer pedido
INSERT INTO pedidos VALUES(ped_seq.NEXTVAL, destino, paqueteria, null, modalidad, usuario, tarjeta);
  
--Obtener el �ltimo pedido de un usuairo
SELECT id_pedido FROM pedidos WHERE id_usuario = 1 AND ROWNUM = 1 ORDER BY id_pedido DESC;

SELECT * FROM carrito WHERE id_usuario = 1;

SELECT p.id_producto, nombre_producto, precio, direccion_imagen, categoria FROM productos p INNER JOIN galerias g ON p.id_producto = g.id_producto WHERE UPPER(categoria) LIKE UPPER('%Vestido%') AND slider = 0

--Ver lo pedido de un usuario
































