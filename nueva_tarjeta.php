<?php

session_start();

include("inc/conexion.php");

$numero = $_POST['tarjeta'];
$exp = $_POST['exp'];
$mes = $_POST['mes'];
$anio = $_POST['anio'];
$tipo = filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING);

if(!isset($_POST['tarjeta'])){
  header("Location:index.php");
  exit;
}

if(!is_numeric($numero) || !is_numeric($exp) || $numero > 99999999999999999999 || $exp > 999){
  header("Location:perfil.php?status=error");
  exit;
}

$sql = "INSERT INTO ref_pago VALUES(tar_seq.NEXTVAL, '$numero', $exp, '$tipo', ".$_SESSION['id']. ", $mes, $anio)";

$query = $conn->prepare($sql);
$query->execute();

header("Location:perfil.php?status=tar_ok");
exit;

?>
